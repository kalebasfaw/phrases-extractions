
import xml.etree.ElementTree as et
doc = et.parse('Dataset/pos_data_set_tuned_tagged_corpus_noun_phrases_orderid.xml')
root = doc.getroot()

sentences = root.findall('sentences/sentence')
sentences_count  = len(list(sentences))
print("stat_pos_data_set_tuned_tagged_corpus_noun_phrases.xml: sentenses_count:{}".format(sentences_count)) 

phrases = root.findall('sentences/sentence/nounphrases/nounphrase/phrase')
phrases_count  = len(list(phrases))
print("stat_pos_data_set_tuned_tagged_corpus_noun_phrases.xml: phrases_count:{}".format(phrases_count)) 

    