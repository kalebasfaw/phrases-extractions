# -*- coding: utf-8 -*-
"""
Created on Sat Nov 23 17:42:12 2019

@author: 
"""

import numpy as np
from scipy.stats import norm
import tensorflow as tf
import keras.backend as K
from keras.models import Model
from keras.layers import Layer, Dense, Embedding, Lambda, Input, Concatenate, Reshape
from keras import metrics
from interfaces import ModelProvider
import constants
from custom_accuracy_metrics import new_sparse_categorical_accuracy as acc


class AutoLabelerEmbeddingModel(ModelProvider):
    '''
    The model will be used to server as embedding block for the input of the autolabler model
    It will be trainder with parsingTreePath input to generate parsing path embedded vectors
    And separately will be trained for 
    '''
    def __init__(self, embeddingDimension,dictionarySize, intermediate1Units, intermediate2Units):
        #The sparse input vector
        sparseInput = Input(shape=(dictionarySize,), name="SparseInput")
        
        #Encoding layer
        intermediate1 = Dense(intermediate1Units,activation='relu')(sparseInput)
        intermediate2 = Dense(intermediate2Units, activation='relu')(intermediate1)
        embeddedVector = Dense(embeddingDimension,activation='relu')(intermediate2)
        
        #Decoding Layer
        decodedIntermediate2Layer = Dense(intermediate2Units, activation='relu')
        decodedIntarmediate1Layer = Dense(intermediate1Units, activation='relu')
        decodedOutputLayer = Dense(dictionarySize, activation='softmax')
     
        output = decodedOutputLayer(
            decodedIntarmediate1Layer(
            decodedIntermediate2Layer(embeddedVector)))
        
        encoder = Model(sparseInput,embeddedVector)
        
        decoder_input = Input(shape=(embeddingDimension,))
        decoderOutput=decodedOutputLayer(
            decodedIntarmediate1Layer(
            decodedIntermediate2Layer(decoder_input)))
        decoder = Model(decoder_input, decoderOutput)
        
        autoEncoder=Model(sparseInput, output)
        
        autoEncoder.compile(optimizer="adam", loss="sparse_categorical_crossentropy", metrics=["categorical_accuracy"])

        self._encoder = encoder;
        super().__init__(autoEncoder)
        
    def getEncoder(self):
        return self._encoder;
        

def main():
    
    provider = AutoLabelerEmbeddingModel(30,5000,20,10)
    model = provider.getModel()
    print(model.summary())
    sparseVector = np.eye(5000)[np.random.choice(5000, 100)]
    fakeLabel = np.random.rand(100)
    model.fit(epochs=20,x=np.array(sparseVector), y=fakeLabel)
if __name__ == '__main__':
    main()        