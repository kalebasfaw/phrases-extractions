# -*- coding: utf-8 -*-
"""
Created on Sat Mar  7 17:32:21 2020

@author: User
"""

from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement, Comment
from xml.dom import minidom
import re
import unicodedata
import sys
#The parsing output file
path = 'Dataset/pos_data_set_tuned_tagged_corpus_noun_phrases_fixed.xml'
#path = 'Dataset/pos_data_set_tuned_tagged_corpus_noun_phrases.xml'
#path = r'F:\临时\天大\Gazetters\分类\Abrsh_samples\pos_data_set_tuned_tagged_corpus_noun_phrases_sample_AB.xml'

#Gazettes
pathVendor = 'Dataset/gazette_data_set_vendor_list1.txt'
pathProduct = 'Dataset/gazette_data_set_product_list1.txt'
pathVersion = 'Dataset/gazette_data_set_version_list1.txt'
pathUpdate = 'Dataset/gazette_data_set_update_list1.txt'
#Auto labling input file
pathRelativePaths = 'Dataset/gazette_data_set_relative_path_noun_phrases.xml'

#Combined gazettes list- gazettes are appended in (vendor, product, version, update) order
gazetteList = []
#Whend a gazette item is appended to the gazette list we keep its index in the gazette index list
gazetteIndexList = []
def buildGazette(gPath):
    stripedList = []
    global gazetteList
    global gazetteIndexList
    with open(gPath, 'r') as file:
        for gl in file.readlines():
            stripedList.append(gl.strip())            
        gazetteList.extend(stripedList)        
        gazetteIndexList.append(len(gazetteList))

#for searchin a phrase in the gazette list and identify the gazette type(vendor, product, version, update)
def gazetteSearch(phrase):
    global gazetteList
    global gazetteIndexList
    try:
        sindex = gazetteList.index(phrase)        
        for gi in range(len(gazetteIndexList)):
            if(gazetteIndexList[gi] > sindex):
                return gi
    except:
        return -1
    
#Calculates the relative path of an entity with respect to a gazette
def calculateRelativePath(gazettePath, entPath):
    gazettePathList = gazettePath.split('-')
    entPathList = entPath.split('-')
    #identifying the common path segment 
    #the smallest length 
    length = len(gazettePathList)
    if length > len(entPathList):
        length = len(entPathList)
    
    commonIndex=0
    #if the two paths has common segment then it should be at the beginning
    #looking for the index at which the two paths differ
    for i in range(length):
        if gazettePathList[i] != entPathList[i]:
            break
        commonIndex = i
    
    #extract the non common gazetter path segment including the last comman 
    relativePathList = gazettePathList[commonIndex:]
    #reverse 
    relativePathList.reverse()
    #extract the non comman entity path segment without the last common
    entPathList = entPathList[commonIndex+1:]
    #Joining the two path segments to formulate the relative path
    #Note that if the the entity is the parent of the gazette entity path list will be empty
    #in this case we will return empty string as a relative path so that the entity could be ignored
    if len(entPathList) > 0:
        relativePathList.extend(entPathList) 
        relativePath = "-".join(relativePathList)
        return re.sub('\d+',"", relativePath)
    return ""
    
#Building the gazettelist and gazetterIndexList    
buildGazette(pathVendor)
buildGazette(pathProduct)
buildGazette(pathVersion)
buildGazette(pathUpdate)



with open(path, encoding="utf-8", errors='ignore') as f:
    contents = f.read()
#encoding='ISO-8859-1', ns_clean=True, recover=True
#xmlString = "".join(ch for ch in contents if unicodedata.category(ch)[0]!="C")
root = ElementTree.fromstring(contents, ElementTree.XMLParser(encoding='utf-8'))
#sentence index
sindex = 1

autoLabelInput = Element('auto_label_input')
sentences = SubElement(autoLabelInput, 'sentences')
a= []
b = []
for parsingsentence in root.iter('sentence'):  
    #if a phrase is identified as a gazette we will ignore all its phrases 
    #Note that we are assuming phrases are enumerated from parent to child
    foundPath="NONE"
    entityList = []
    gazettEntityTuples = [None, None, None, None]
    gazettEntityList_add = [[], [], [], []]
    for nounPhrases in parsingsentence.iter('nounphrases'):
        for nounPhrase in nounPhrases.iter('nounphrase'):
            x = nounPhrase.findall('path')
            path = x[0].text
            prefix = path[:len(foundPath)]
            if(prefix == foundPath):
                continue        
            x = nounPhrase.findall('phrase')
            try:
                phrase = x[0].text
                a.append(phrase)
#                print(phrase)
#                if 'Tooltalk' in phrase:
#                    sys.exit(0)
                phrase = phrase.replace('  ',' ')
                gindex = gazetteSearch(phrase.strip())
#                print(gindex)
            except Exception:
#                continue
                phrase = ''
                gindex = -1
            if(gindex == -1):
                entityList.append((path, phrase))
            else:
                if gazettEntityTuples[gindex] == None:
                    gazettEntityTuples[gindex] = (path, phrase)
                    foundPath = path
                    entityList[:] = [(x, y) for x, y in entityList if calculateRelativePath(path, x) != ""]
                else:
                    gazettEntityList_add[gindex].append([path, phrase])
                    foundPath = path
                    entityList[:] = [(x, y) for x, y in entityList if calculateRelativePath(path, x) != ""]                    
    sindex+=1
    if(sindex % 1000 == 0):
        print(sindex)        
         
    if gazettEntityTuples.count(None) ==  4:    
        continue
    #Creating a sentence node and setting the id subElement
    sentence = SubElement(sentences, 'sentence')
    sentenceId = SubElement(sentence, 'id')
    sentenceId.text = str(sindex)
    
    gazettes = SubElement(sentence, 'gazettes')
    entities = SubElement(sentence, 'entities')
    for i in range(len(gazettEntityTuples)):
        if gazettEntityTuples[i] is not None:
            gazette = SubElement(gazettes, 'gazette')
            gazetteid = SubElement(gazette, 'gazette_id')  
            gazetteid.text = str(i)
            gazettePhrase = SubElement(gazette, 'phrase')
            _,gPhrase = gazettEntityTuples[i]
            gazettePhrase.text = gPhrase
        if gazettEntityList_add[i] is not []:
            for j in gazettEntityList_add[i]:
                gazette = SubElement(gazettes, 'gazette')
                gazetteid = SubElement(gazette, 'gazette_id') 
                gazetteid.text = str(i)
                gazettePhrase = SubElement(gazette, 'phrase')
                [_,gPhrase] = j
                gazettePhrase.text = gPhrase
#            gazette = SubElement(gazettes, 'gazette')
#            gazetteid = SubElement(gazette, 'gazette_id')  
#            gazetteid.text = str(2)
#            gazettePhrase = SubElement(gazette, 'phrase')
#            gPhrase = 'hanlinyi'
#            gazettePhrase.text = gPhrase
       
    for entPath, entPhrase in entityList:
        entity = SubElement(entities, 'entity')
        entityphrase = SubElement(entity, 'ent_phrase')
        entityphrase.text = entPhrase
        absolutePath = SubElement(entity, 'absolute_path')
        absolutePath.text = re.sub('\d+',"", entPath)
        relativePaths = SubElement(entity, 'relative_paths')
        for i in range(len(gazettEntityTuples)):
            if gazettEntityTuples[i] is not None:
                relativePath = SubElement(relativePaths, 'relative_path')
                gazetteid = SubElement(relativePath, 'gazette_id')  
                gazetteid.text = str(i)
                calculatedRelativePath = calculateRelativePath(gazettEntityTuples[i][0], entPath)
                entityRelativePath = SubElement(relativePath, 'path')
                entityRelativePath.text = calculatedRelativePath
            if gazettEntityList_add[i] is not []:
                for j in gazettEntityList_add[i]:
                    relativePath = SubElement(relativePaths, 'relative_path')
                    gazetteid = SubElement(relativePath, 'gazette_id') 
                    gazetteid.text = str(i)
                    calculatedRelativePath = calculateRelativePath(j[0], entPath)
                    entityRelativePath = SubElement(relativePath, 'path')
                    entityRelativePath.text = calculatedRelativePath

file = open(pathRelativePaths, 'w')
rough_string = ElementTree.tostring(autoLabelInput, 'utf-8')
reparsed = minidom.parseString(rough_string)
reparsedString = reparsed.toprettyxml(indent="  ")
file.write(reparsedString)   

    