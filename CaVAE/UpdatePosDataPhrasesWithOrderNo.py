# -*- coding: utf-8 -*-
"""
Created on Sun Jan 17 11:27:06 2021

@author: User
"""
import numpy as np
from xml.etree import ElementTree
from bs4 import BeautifulSoup as bs


path = 'Dataset/pos_data_set_tuned_tagged_corpus_noun_phrases_fixed.xml'
pathOut = 'Dataset/pos_data_set_tuned_tagged_corpus_noun_phrases_orderid.xml'
tree = ElementTree.parse(path)
root = tree.getroot()

id = 1
for s in root.findall("sentences/sentence"):
    for nps in s.findall("nounphrases"):
        for np in nps.findall("nounphrase"):
            phrase_id = ElementTree.Element("id")
            phrase_id.text = str(id)
            np.append(phrase_id)
            #np.set('OrderNo', str(orderNumber))
            id = id + 1

tree.write(pathOut)
#mydata = ElementTree.tostring(root).decode("utf-8") 
#myfile = open(pathOut, "w")
#myfile.write(mydata)
#myfile.close()