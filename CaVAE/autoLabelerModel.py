# -*- coding: utf-8 -*-
"""
Created on Sat Nov 23 17:42:12 2019

@author: 
"""

import numpy as np
import tensorflow.keras.backend as K
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense,  Lambda, Input
from tensorflow.keras import losses
from interfaces import ModelProvider
import constants
import matplotlib.pyplot as plt
import mixedModeRandomData as randomSample



    
class AutoLabelerModel(ModelProvider):
    def sampling(self, args):
        z_mean, z_log_sigma = args
        epsilon = K.random_normal(shape=(K.shape(z_mean)[0], self._latentDim), mean=0., stddev=0.1)
        return z_mean + K.exp(z_log_sigma) * epsilon
    
    def __init__(self, embeddingDimension, noOfGazetteEntities, noOfClusters, intermediateDim, latentDim):
        self._noOfClusters = noOfClusters        
        self._originalDim = (noOfGazetteEntities+1)* embeddingDimension
        self._latentDim = latentDim
        
        inputs = Input(shape=(self._originalDim,))
        h = Dense(intermediateDim, activation='relu')(inputs)
        z_mean = Dense(latentDim)(h)
        z_log_sigma = Dense(latentDim)(h)

        # Use sampling to generate the encoded output
        z = Lambda(self.sampling)([z_mean, z_log_sigma])

        # Create encoder
        encoder = Model(inputs, [z_mean, z_log_sigma, z], name='encoder')        
        
        # Create decoder
        latent_inputs = Input(shape=(latentDim,), name='z_sampling')
        x = Dense(intermediateDim, activation='relu')(latent_inputs)
        outputs = Dense(self._originalDim, activation='sigmoid')(x)
        decoder = Model(latent_inputs, outputs, name='decoder')
        
        # instantiate VAE model
        outputs = decoder(encoder(inputs)[2])
        vae = Model(inputs, outputs, name="vae")
        
        # define the loss function
        reconstruction_loss = losses.binary_crossentropy(inputs, outputs)
        reconstruction_loss *= self._originalDim
        kl_loss = 1 + z_log_sigma - K.square(z_mean) - K.exp(z_log_sigma)
        kl_loss = K.sum(kl_loss, axis=-1)
        kl_loss *= -0.5
        vae_loss = K.mean(reconstruction_loss + kl_loss)
        
        # add loss to vae model
        vae.add_loss(vae_loss)
        
        vae.compile(optimizer="adam")
        self._encoder = encoder
        self._decoder = decoder
        super().__init__(vae)
        
    def getEncoder(self):
        return self._encoder
    
    def getDecoder(self):
        return self._decoder

def main():
    provider = AutoLabelerModel(30, 4,3, 15, 2)
    model = provider.getModel()
    print(model.summary())
 
    '''
    import autoLabelerInput as inputs
    inputPath = 'D:/private/DeepLearning/SampleData/gazette_data_set_relative_path_noun_phrases_sample.xml'
    labelPath = 'D:/private/DeepLearning/SampleData/20201129_LabledEntities.csv'
    (x_train,y_train),(x_test, y_test) = inputs.getLabeledInput(inputPath, labelPath)
    provider = AutoLabelerModel(constants.SYNTAX_PATH_VECTOR_DIMENSION,4,3, 25, 3) #constants.LATENT_DIMENSION)
    model = provider.getModel()
    encoder = provider.getEncoder()
    print(model.summary())
    
    # Train the model
    model.fit(x_train, epochs=100, batch_size=32, validation_data=(x_test,))
    
    # Predict the encoded output of the test samples 
    x_test_encoded = encoder.predict(x_test, batch_size=32)[2]
    
    # Scatter the encoded output to see if clustering occurs (Note that the latent space is 2 dimensional)
    plt.figure(figsize=(6,6))
    plt.scatter(x_test_encoded[:,0], x_test_encoded[:, 1],c=y_test)
    plt.colorbar()
    plt.show()
    '''   
if __name__ == '__main__':
    main()        