# -*- coding: utf-8 -*-
"""
Created on Sat Nov 23 17:42:12 2019

@author: 
"""

import numpy as np
from scipy.stats import norm
import keras.backend as K
from keras.models import Model
from keras.layers import Layer, Dense, Embedding, Lambda, Input, Concatenate, Flatten
from keras import metrics
from interfaces import ModelProvider
import constants

def sampling(args):
    z_mean,z_log_var = args
    latent_dim = constants.LATENT_DIMENSION
    epsilon = K.random_normal(shape=(K.shape(z_mean)[0], latent_dim), mean=0., stddev=1.0)
    return z_mean + K.exp(z_log_var/2)*epsilon


class CustomVariationalLayer(Layer):

    """ Identity transform layer that adds KL divergence
    to the final model loss.
    """

    def __init__(self, original_dim, z_mean, z_log_var, **kwargs):
        self._orginal_dim = original_dim
        self._z_mean = z_mean
        self._z_log_var = z_log_var
        self.is_placeholder = True
        super(CustomVariationalLayer, self).__init__(**kwargs)

    def vae_loss(self, x, x_decoded_mean):
        xent_loss = self._orginal_dim * metrics.binary_crossentropy(x, x_decoded_mean)
        kl_loss = - 0.5 * K.sum(1 + self._z_log_var - K.square(self._z_mean) - K.exp(self._z_log_var), axis=-1)
        return K.mean(xent_loss + kl_loss)
    
    def call(self, inputs):
        x = inputs[0]
        x_decoded_mean = inputs[1]
        loss = self.vae_loss(x, x_decoded_mean)
        self.add_loss(loss, inputs=inputs)
        return x
    
class AutoLabelerModel(ModelProvider):
    '''
    The model consists of three blocks of keras layers:
        1. The feature extraction block with two embedding layers to extract embedded vectors
            with tokens from the parsing tree path, and relative paths from input phrase to 
            gazette named entities in the parsing tree (from the sentence)
            The block also consists of a bidirectional LSTM layer for extracting features
            the ngrams constituting the in put phrase. Finally the block produces features vector
            by concatenating the 2 embedding vectors and LSTM output
        2. The GMM block consists of two hidden layers connected to the GMM parameters and mixing
            coefficient layers
        3. A custom cost function based on log probability will be used to train the model and obtain
            metrics
    '''
    def __init__(self, embeddingDimension, parsingTreePathDictionarySize, relativePathDictionarySize,
                 noOfGazetteEntities, noOfClusters, gmmHiddenLayerUnits, latentDimension):
        self._noOfClusters = noOfClusters
        #The input layer
        parsingPathToken = Input(shape=(1,), name="ParsingPathToken")
        relativePathTokens = Input(shape=(noOfGazetteEntities,), name="RelativePathTokens")

        #Feature Extraction Block
        parsingPathEmbedding = Embedding(parsingTreePathDictionarySize,
                                         embeddingDimension,
                                         mask_zero=False,
                                         input_length=1,
                                         name="ParsingPathEmbedding")
        relativePathEmbedding = Embedding(relativePathDictionarySize,
                                          embeddingDimension,
                                          mask_zero=False,
                                          input_length=noOfGazetteEntities,
                                          name="RelativePathEmbedding")
        parsingPathEmbeddedVector = parsingPathEmbedding(parsingPathToken)
        relativePathEmbeddedVector = relativePathEmbedding(relativePathTokens)

        gmmInput = Concatenate(axis=1)([parsingPathEmbeddedVector, relativePathEmbeddedVector])
        gmmInput = Flatten()(gmmInput)
        print(K.shape(gmmInput))

        original_dim = (noOfGazetteEntities+1)*embeddingDimension
        #GMM Block
#        gmmHidden1 = Dense(gmmHiddenLayerUnits, activation='relu')(gmmInput)
#        gmmHidden2 = Dense(gmmHiddenLayerUnits, activation='relu')(gmmHidden1)
        gmmHidden2   = Dense(gmmHiddenLayerUnits, activation='relu')(gmmInput)

        zDim = latentDimension
        z_mean = Dense(zDim)(gmmHidden2)
        z_log_var = Dense(zDim, activation=K.exp)(gmmHidden2)
        z = Lambda(sampling, output_shape=(zDim,))([z_mean,z_log_var])
        
        decoder_h = Dense(gmmHiddenLayerUnits, activation='relu')
        decoder_mean = Dense(original_dim, activation='sigmoid')
        
        h_decoder = decoder_h(z)
        x_decoded_mean = decoder_mean(h_decoder)
        
        y = CustomVariationalLayer( original_dim, z_mean, z_log_var)([x, x_decoded_mean])
        #encoder model includes the 
        model = Model(gmmInput, y)
        model.compile(optimizer="rmsprop", loss=None)

        super().__init__(model)

def main():
#    parsingToken = np.random.randint(0, high=25, size=100)
#    relativeToken = np.random.randint(0,high=20, size=(100,4))
#    ngramFeatures = np.random.randint(0,high=81, size=(100,20))
    '''
    embeddingDimension, parsingTreePathDictionarySize, relativePathDictionarySize,
    noOfGazetteEntities, noOfClusters, gmmHiddenLayerUnits):
    '''
    
    provider = AutoLabelerModel(5,20,25,4,3, 6, constants.LATENT_DIMENSION)
    model = provider.getModel()
    print(model.summary())

if __name__ == '__main__':
    main()        