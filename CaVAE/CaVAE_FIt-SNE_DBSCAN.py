#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 18 17:20:51 2021

@author: 
"""
# =============================================================================
# Please note: you can modify the data source by modifying 22 rows, and modify the iteration number of model training by modifying 198 rows.
# =============================================================================

# import pandas as pd
# #data = pd.read_csv(r'./111.csv')
# standard = pd.read_csv('./Dataset/20201129_LabledEntities.csv')
# standard_label_0 = standard[standard['Class']==0]
# sentence_label_0 = standard_label_0['Phrase'].tolist()

import re



with open('./Dataset/gazette_data_set_relative_path_noun_phrases_sample_distinct_vpv_phrase_orderNo.xml', 'r') as f1:
    list1 = f1.readlines()

lines_label_0 = []
n = len(list1)
for i in range(n):
    if i%5000==1:
        print(i)
    line = list1[i]
    searchObj = re.search( r'<ent_phrase>(.*)</ent_phrase>', line, re.M|re.I)
    if searchObj:
        if ('them' in searchObj.group(1)) or ('they' in searchObj.group(1)) or ('it' in searchObj.group(1)) or ('the ' in searchObj.group(1)):
            lines_label_0.append(i)
remove = []

for i in lines_label_0:
    print(i)
    temp_i = i
    start = -1
    end = -1
    while temp_i>0:
        temp_i-=1
        if 'entity OrderNo' in list1[temp_i]:
            start = temp_i
            break
    temp_i = i
    while temp_i<n+1:
        temp_i+=1
        if '</entity>' in list1[temp_i]:
            end = temp_i
            break
    remove.append([start,end])
res = []
temp = remove.pop(0)
i=0
while i < n:
    if i%5000==1:
        print(i)
    if i!=temp[0]:
        res.append(list1[i])
        i+=1
    else:
        i = temp[1]+1
        if remove:
            temp = remove.pop(0)
f=open("./Dataset/remove_label_0.xml","w")
f.writelines(res)
f.close()





def evaluate(path, labelPath):
    """    
    Trains the AutoLabelerGveaModel with the provided training data
    Parameters
    ----------
    path : TYPE. string TYPE.
        DESCRIPTION.
    Spefify the path to the xml file containing the selected for training noun-phrases 
        DESCRIPTION.
    labelPath: TYPE. string TYPE.
        DESCRIPTION.
    specify the path to the csv file containing noun-phrase to label mapping
    Returns
    -------
    None.

    """
    intermediate_dim = 599
    latent_dim = 200
    M = 3
    N = 50
    #load test data
    corpus = XMLLabeledPathFile(path, labelPath).getCorpus()
    inData = LabeledAutoLabelInut(corpus, (1,0,0),LabeledAutoLabelerPresenterFactory, None, pathVocabularySize=5000)
    inData.initializeWithDictionary('Dataset/autolabeler_')
    absDictionarySize = inData.getPathDictionarySize()
    relDictionarySize = inData.getRelativePathDictionarySize()
    
    # deserialize encoder model from JSON
    autolabelerEncoderModelFileName = "ALEncoder_model"
    provider = AutoLabelerGvaeModel(absDictionarySize, relDictionarySize, 4, intermediate_dim, latent_dim, M, N )
    encoder = provider.getEncoder()

    _loadWeights(encoder, autolabelerEncoderModelFileName)   
    

    x_evaluation, y_evaluation = _getLabeledInput(inData, trainTestSplit=False)
    abs_encoded_evaluation, rel_encoded_evaluation = encoder.predict(x_evaluation)
    rel_encoded_evaluation = np.reshape(rel_encoded_evaluation, (-1,4*M*N))
    encoded_evaluation = np.hstack((abs_encoded_evaluation,rel_encoded_evaluation))
    
    from sklearn.manifold import TSNE
    tsne = TSNE(metric='hamming')
    viz = tsne.fit_transform(encoded_evaluation)
    
    plt.figure(figsize=(6,6))
    plt.scatter(viz[:,0], viz[:, 1],c=y_evaluation)
    plt.colorbar()
    plt.show()

def _getLabeledInput(inputDataContainer, trainTestSplit = True):
    absDictionarySize = inputDataContainer.getPathDictionarySize()
    relDictionarySize = inputDataContainer.getRelativePathDictionarySize()
    trainigPresenter = inputDataContainer.getTrainingData()
    testPresenter = inputDataContainer.getTestData()
    trBatches = trainigPresenter.nBatches()
    tBatches = testPresenter.nBatches()
    
    batchSise = constants.BATCH_SIZE
    rows = (trBatches+tBatches)*batchSise
    all_abs_inputs = np.zeros((rows,absDictionarySize))
    all_rel_inputs = np.zeros((rows, 4, relDictionarySize))
    all_output = np.zeros((rows,))
    offset = 0;
    for i in range(trBatches):
        [batchxa, batchxr], batchy = trainigPresenter.__next__()
        rows = batchxa.shape[0]
        all_abs_inputs[offset:offset+rows] = batchxa
        all_rel_inputs[offset:offset+rows] = batchxr
        all_output[offset:offset+rows] = batchy
        offset += rows
    cutoff = offset
    for i in range(tBatches):
        [batchxa, batchxr], batchy = testPresenter.__next__()
        rows = batchxa.shape[0]
        if rows == 0:
            break
        all_abs_inputs[offset:offset+rows] = batchxa
        all_rel_inputs[offset:offset+rows] = batchxr
        all_output[offset:offset+rows] = batchy
        offset += rows
    if trainTestSplit :
        x_train_abs = all_abs_inputs[0:cutoff]
        x_train_rel = all_rel_inputs[0:cutoff]
        x_train = [x_train_abs, x_train_rel]
        y_train = all_output[0:cutoff]
        x_test_abs = all_abs_inputs[cutoff:]
        x_test_rel = all_rel_inputs[cutoff:]
        x_test = [x_test_abs, x_test_rel]
        y_test = all_output[cutoff:]        
        return ((x_train, y_train),(x_test,y_test))
    else:
        x_train = [all_abs_inputs, all_rel_inputs]
        y_train = all_output
        return (x_train,y_train)

def _saveModelWeights(model, fileName):
    # serialize weights to HDF5
    model.save_weights("TrainedModels/"+fileName+".h5")

def _loadWeights(model, fileName):
    model.load_weights("TrainedModels/"+fileName+".h5")





import numpy as np
import matplotlib.pyplot as plt

from autoLabelerInput1 import XMLLabeledPathFile, LabeledAutoLabelInut, LabeledAutoLabelerPresenterFactory
from AutoLablerGveaModel import AutoLabelerGvaeModel
# import tsne_dbscan_rf as tdr
import sys; sys.path.append('/home/hanlinyi/下载/VAE_Code/FIt-SNE')
import constants
# from fast_tsne import fast_tsne

path = 'Dataset/remove_label_0.xml'
intermediate_dim = 599
latent_dim = 200
M = 3
N = 50

epochs = 100
batch_size = constants.BATCH_SIZE

corpus = XMLLabeledPathFile(path).getCorpus()
inData = LabeledAutoLabelInut(corpus, (.8,0,.2),LabeledAutoLabelerPresenterFactory, None, pathVocabularySize=5000)
inData.initializeEmptyPathInput()

(x_train, _), (x_test, y_test) = _getLabeledInput(inData)

absDictionarySize = inData.getPathDictionarySize()
relDictionarySize = inData.getRelativePathDictionarySize()

# train the auto-labeler
provider = AutoLabelerGvaeModel(absDictionarySize, relDictionarySize, 4, intermediate_dim, latent_dim, M, N )
model = provider.getModel()
encoder = provider.getEncoder()
for e in range(epochs):
    model.fit(x_train, x_train, shuffle=True, epochs=1, batch_size=batch_size,
            validation_data=(x_test, x_test))
    provider.updateTau(e)

abs_encoded_train, rel_encoded_train = encoder.predict(x_train)
rel_encoded_train = np.reshape(rel_encoded_train, (-1, 4*M*N))
encoded_train = np.hstack((abs_encoded_train, rel_encoded_train))

dataset = encoded_train
U, s, V = np.linalg.svd(dataset, full_matrices=False)
X50 = np.dot(U, np.diag(s))[:,:50]

#from openTSNE import TSNE

autolabelerEncoderModelFileName = "ALEncoder_model"
_saveModelWeights(encoder, autolabelerEncoderModelFileName)    
inData.save('Dataset/autolabeler_')









def evaluate(path, labelPath):

    intermediate_dim = 599
    latent_dim = 200
    M = 3
    N = 50
    #load test data
    corpus = XMLLabeledPathFile(path, labelPath).getCorpus()
    inData = LabeledAutoLabelInut(corpus, (1,0,0),LabeledAutoLabelerPresenterFactory, None, pathVocabularySize=5000)
    inData.initializeWithDictionary('Dataset/autolabeler_')
    absDictionarySize = inData.getPathDictionarySize()
    relDictionarySize = inData.getRelativePathDictionarySize()
    
    # deserialize encoder model from JSON
    autolabelerEncoderModelFileName = "ALEncoder_model"
    provider = AutoLabelerGvaeModel(absDictionarySize, relDictionarySize, 4, intermediate_dim, latent_dim, M, N )
    encoder = provider.getEncoder()

    _loadWeights(encoder, autolabelerEncoderModelFileName)   
    

    x_evaluation, y_evaluation = _getLabeledInput(inData, trainTestSplit=False)
    abs_encoded_evaluation, rel_encoded_evaluation = encoder.predict(x_evaluation)
    rel_encoded_evaluation = np.reshape(rel_encoded_evaluation, (-1,4*M*N))
    encoded_evaluation = np.hstack((abs_encoded_evaluation,rel_encoded_evaluation))
    
    from sklearn.manifold import TSNE
    tsne = TSNE(metric='hamming')
    viz = tsne.fit_transform(encoded_evaluation)
    
    plt.figure(figsize=(6,6))
    plt.scatter(viz[:,0], viz[:, 1],c=y_evaluation)
    plt.colorbar()
    plt.show()

def _getLabeledInput(inputDataContainer, trainTestSplit = True):
    absDictionarySize = inputDataContainer.getPathDictionarySize()
    relDictionarySize = inputDataContainer.getRelativePathDictionarySize()
    trainigPresenter = inputDataContainer.getTrainingData()
    testPresenter = inputDataContainer.getTestData()
    trBatches = trainigPresenter.nBatches()
    tBatches = testPresenter.nBatches()
    
    batchSise = constants.BATCH_SIZE
    rows = (trBatches+tBatches)*batchSise
    all_abs_inputs = np.zeros((rows,absDictionarySize))
    print('$$$$$$$$$$$$$$$$$$',rows,batchSise,trBatches,tBatches)
    all_rel_inputs = np.zeros((rows, 4, relDictionarySize))
    all_output = np.zeros((rows,))
    offset = 0;
    for i in range(trBatches):
        [batchxa, batchxr], batchy = trainigPresenter.__next__()
        rows = batchxa.shape[0]

        all_abs_inputs[offset:offset+rows] = batchxa
        all_rel_inputs[offset:offset+rows] = batchxr
        all_output[offset:offset+rows] = batchy
        offset += rows
    cutoff = offset
    j=0
    for i in range(tBatches):
        j+=1
        [batchxa, batchxr], batchy = testPresenter.__next__()
        rows = batchxa.shape[0]
        if rows == 0:
            break
        all_abs_inputs[offset:offset+rows] = batchxa
        all_rel_inputs[offset:offset+rows] = batchxr
        all_output[offset:offset+rows] = batchy
        offset += rows
    if trainTestSplit :
        x_train_abs = all_abs_inputs[0:cutoff]
        x_train_rel = all_rel_inputs[0:cutoff]
        x_train = [x_train_abs, x_train_rel]
        y_train = all_output[0:cutoff]
        x_test_abs = all_abs_inputs[cutoff:]
        x_test_rel = all_rel_inputs[cutoff:]
        x_test = [x_test_abs, x_test_rel]
        y_test = all_output[cutoff:]        
        return ((x_train, y_train),(x_test,y_test))
    else:
        x_train = [all_abs_inputs, all_rel_inputs]
        y_train = all_output
        return (x_train,y_train)

def _saveModelWeights(model, fileName):

    model.save_weights("TrainedModels/"+fileName+".h5")

def _loadWeights(model, fileName):
    model.load_weights("TrainedModels/"+fileName+".h5")

def get_explanatory_importances(dataset,clusters,feature_names):

    labels = np.zeros(dataset.shape[0])
    for idx, cluster in enumerate( clusters ):
        labels[cluster] = idx


    rf = Pipeline([('scaler',StandardScaler()),('rf',RandomForestClassifier())])
    rf.fit( dataset, labels )
    predictions = rf.predict( dataset )



    return sorted(zip(rf['rf'].feature_importances_,feature_names))[::-1], predictions, rf

#First, read the VAE model and replace line 122('Dataset/gazette_data_set_relative_path_noun_phrases_sample.xml') with the test data

import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
from autoLabelerInput1 import XMLLabeledPathFile, LabeledAutoLabelInut, LabeledAutoLabelerPresenterFactory,AutoLableInput
from AutoLablerGveaModel import AutoLabelerGvaeModel
import sys; sys.path.append('/home/hanlinyi/下载/VAE_Code/FIt-SNE')
import constants
from fast_tsne import fast_tsne


#path = 'Dataset/gazette_data_set_relative_path_noun_phrases_sample_0.8.8.3_two.xml'
path = 'Dataset/remove_label_0.xml'
#labelPath = 'Dataset/20201129_LabledEntities.csv'
intermediate_dim = 599
latent_dim = 200
M = 3
N = 50


batch_size = constants.BATCH_SIZE

corpus = XMLLabeledPathFile(path).getCorpus()
inData = LabeledAutoLabelInut(corpus, (.8,0,.2),LabeledAutoLabelerPresenterFactory, None, pathVocabularySize=5000)
#a = inData.getTrainingData()
#dir(a)

#[batchxa, batchxr], batchy = a.__sizeof__()



from csv import reader
from xml.etree import ElementTree
def loadLabels(labelsPath):
    with open(labelsPath, 'r') as f:
        csvReader = reader(f)
        listOfLabels = list(csvReader)
    # create a dictionary with (SentenceId,OrderNo)
    dictionary = {(r[0],r[1]):r[3] for r in listOfLabels[1:]}
    return dictionary
#labels = loadLabels(labelPath)
root = ElementTree.parse(path).getroot()
_corpus = []        
gazette= []
for sentence in root.findall('sentences/sentence'):
    sentenceId = sentence.find('id').text
    entityList = []
    for entity in sentence.findall('gazettes/gazette'):
        absolutePath = entity.find('phrase').text
        gazette.append(absolutePath)
    for entity in sentence.findall('entities/entity'):
        absolutePath = entity.find('ent_phrase').text
        orderNo = entity.attrib['OrderNo']
        for relativePath in entity.findall('relative_paths/relative_path'):
            pathTuple = [None, None, None, None]
            gazetteId = int(relativePath.find('gazette_id').text)
            
            currentPath = relativePath.find('path').text
            pathTuple[gazetteId] = currentPath


            label = 0
            
        entityList.append(((absolutePath, pathTuple),int(label)))           
    _corpus.append(entityList)   
aa = _corpus
corpus_all_sentence = []
for i in _corpus:
    corpus_all_sentence.extend(i)
corpus_all_sentence = [i[0][0] for i in corpus_all_sentence]


for i in corpus_all_sentence:
    if i is None:
        print(11)



ss = 0
for i in corpus:
    ss = ss + len(i)



inData.initializeEmptyPathInput()




(x_train, _)= _getLabeledInput(inData,trainTestSplit = False)
absDictionarySize = inData.getPathDictionarySize()
relDictionarySize = inData.getRelativePathDictionarySize()


provider = AutoLabelerGvaeModel(absDictionarySize, relDictionarySize, 4, intermediate_dim, latent_dim, M, N )
model = provider.getModel()
encoder = provider.getEncoder()
_loadWeights(encoder, "ALEncoder_model")
abs_encoded_train, rel_encoded_train = encoder.predict(x_train)
rel_encoded_train = np.reshape(rel_encoded_train, (-1, 4*M*N))
encoded_train = np.hstack((abs_encoded_train, rel_encoded_train))    




#At this point, the model has been read and the test text has been mapped using the model.
dataset = encoded_train
U, s, V = np.linalg.svd(dataset, full_matrices=False)
X50 = np.dot(U, np.diag(s))[:,:50]



# from openTSNE import TSNE


X_embed = fast_tsne(X50, late_exag_coeff=4)
# X_embed_opentsne = TSNE().fit(X50)
# X_embed = X_embed_opentsne
from sklearn.cluster import DBSCAN
import pandas as pd
clusters = []
for i in range(1,6):
    for j in range(1,6):
        
        dbscan = DBSCAN(i, j)
        clustering = dbscan.fit( X_embed )
        for i in range(np.min(clustering.labels_),np.max(clustering.labels_)+1):
            c= np.argwhere( clustering.labels_ == i ).flatten().tolist()
            clusters.append( c )
        
        del dbscan
        del clustering
        
        clusters.sort(key=len)
        X_embedded=X_embed
        clusters=clusters[::-1]
        
        cluster_num = []
        index_num = []
        dotsize=2
        fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
        fig.set_figwidth(12)
        ax1.scatter(X_embedded[:,0],X_embedded[:,1], alpha=1, s=dotsize)
        ax1.set_title('t-SNE Embedding of Data', fontsize=20)
        for idx, cluster in enumerate( clusters ):
            if idx in [1,2,3,4,5,6]:
                ax2.scatter(X_embedded[cluster,0],X_embedded[cluster,1], alpha=1,label="cluster %s" % (idx), s=dotsize)
                for i in cluster:
                    cluster_num.append(idx)
                    index_num.append(i)
                    
            else:
                for i in cluster:
                    cluster_num.append(-1)
                    index_num.append(i)    
                ax2.scatter(X_embedded[cluster,0],X_embedded[cluster,1], alpha=1,c = "black",s=dotsize)
        ax2.set_title("Cluster Labels from DBSCAN", fontsize=20)
        
        ax1.axis('off')
        ax2.axis('off')
        lgnd = ax2.legend(bbox_to_anchor=(1.04,1), borderaxespad=0, prop={"size":12})
        for handle in lgnd.legendHandles:
            handle.set_sizes([40.0])
        
        fig.tight_layout()
        plt.show()        
                
                
        a = pd.DataFrame()
        cluster_num = [i + 1 if i!= -1 else i for i in cluster_num]
        a['cluster'] =  cluster_num[len(cluster_num)-len(corpus_all_sentence):]
        a['index'] =  index_num[len(cluster_num)-len(corpus_all_sentence):]
        
        a['sentence'] = corpus_all_sentence
        
        
        a.to_csv('./111'+'_'+str(i)+'_'+str(j)+'.csv',index = None)
    
















#
#

