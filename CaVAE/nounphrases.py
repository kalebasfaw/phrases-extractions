
import xml.etree.ElementTree as et
root = et.parse('pos_data_set_tuned_tagged_corpus_noun_phrases_fixed.xml').getroot()

#
# Count number of noun phrases
#
phrases = root.findall('sentences/sentence/nounphrases/nounphrase/phrase')
count  = len(list(phrases))
print("noun phrases count:{}".format(count))
#for phrase in phrases:
#    print(phrase.text)