cat Dataset_sample/pos_data_set_tuned_tagged_corpus_noun_phrases.xml \
| grep -v '<\?xml' \
| grep -v '<parsing>' \
| grep -v '</parsing>' \
> tmp.xml

echo '<parsing>' > Dataset_sample/pos_data_set_tuned_tagged_corpus_noun_phrases_fixed.xml
cat tmp.xml >> Dataset_sample/pos_data_set_tuned_tagged_corpus_noun_phrases_fixed.xml
echo '</parsing>' >> Dataset_sample/pos_data_set_tuned_tagged_corpus_noun_phrases_fixed.xml
rm -rf tmp.xml