
import xml.etree.ElementTree as et
import collections
import csv
import settings
pos_data_doc = et.parse(settings.pos_data_orderid_file_name)
pos_data_root = pos_data_doc.getroot()

gazette_data_doc = et.parse(settings.gazette_data_file_name)
gazette_data_root = gazette_data_doc.getroot()

pos_data_phrases = pos_data_root.findall('sentences/sentence/nounphrases/nounphrase')
gazette_data_phrases = gazette_data_root.findall('sentences/sentence/entities/entity')
'''
matched_phrases_count=0
for pos_phrase in pos_data_phrases:
        for gazette_phrase in gazette_data_phrases:
                if pos_phrase.find("id").text == gazette_phrase.find("id").text:
                        matched_phrases_count+=1
                        #print("pos_data_phrase found in gazette_data_phrases:_JA_: {},{}".format(pos_phrase.find("id").text , pos_phrase.find("phrase").text )) 
                #else:
                #        print("post_data_phrase found in gazette_data_phrases:_NO__: {}".format(pos_phrase.text)) 
print("stat_pos_data_phrases_found_in_gazette_data_phrases: phrases_count:{}".format(matched_phrases_count)) 
print("stat_pos_data_phrases_not_found_in_gazette_data_phrases: phrases_count:{}".format(len(list(pos_data_phrases))-matched_phrases_count)) 
'''
pos_data_phrases_list = [x.text for x in pos_data_root.findall('sentences/sentence/nounphrases/nounphrase/phrase')]
print("How many terminal noun phrases you obtain from these TVDs? :{}".format(len(list(pos_data_phrases_list)))) 
#print(pos_data_phrases_list)

pos_data_phrases_bins = collections.Counter(pos_data_phrases_list).most_common()
print("How many terminal noun phrases are distinct? :{}".format(len(pos_data_phrases_bins)))

pos_data_phrases_grouped = [x[0] for x in pos_data_phrases_bins]
#print(pos_data_phrases_grouped)

f_pos_data_phrases_grouped = open("Dataset_sample/pos_data_phrases_grouped.csv", "w")
w_pos_data_phrases_grouped = csv.writer(f_pos_data_phrases_grouped)

f_pos_data_phrases_overlapped = open("Dataset_sample/pos_data_phrases_overlapped.csv", "w")
w_pos_data_phrases_overlapped = csv.writer(f_pos_data_phrases_overlapped)
c_pos_data_phrases_overlapped = 0

f_pos_data_phrases_unique = open("Dataset_sample/pos_data_phrases_unique.csv", "w")
w_pos_data_phrases_unique = csv.writer(f_pos_data_phrases_unique)
c_pos_data_phrases_unique = 0

#pos_data_phrases_list2=[]
for p1 in pos_data_phrases_grouped:
        count = 0
        matchs = []
        for p2 in pos_data_phrases_grouped:
                 if (p1 is not None) and (p2 is not None):
                        if p1 in p2:
                                count+=1
                                matchs.append(p2)
                                if count > 1:
                                        break
        #print(matchs)
        pos_data_phrases = (p1,count,matchs)
        #pos_data_phrases_list2.append(pos_data_phrases)
        w_pos_data_phrases_grouped.writerow(pos_data_phrases)
        if(count == 1): 
                c_pos_data_phrases_unique += 1
                w_pos_data_phrases_unique.writerow(pos_data_phrases)
        if(count >= 2): 
                c_pos_data_phrases_overlapped += 1
                w_pos_data_phrases_overlapped.writerow(pos_data_phrases)

f_pos_data_phrases_grouped.close()
f_pos_data_phrases_overlapped.close()
f_pos_data_phrases_unique.close()

print("How many terminal noun phrases overlap with at least one other terminal noun phrases? :{}".format(c_pos_data_phrases_overlapped))
print("How many terminal noun phrases are unique? :{}".format(c_pos_data_phrases_unique)) 

#pos_data_phrases_overlapped = [x for x in pos_data_phrases_list2 if x[1] >= 2]
#print("How many terminal noun phrases overlap with at least one other terminal noun phrases? :{}".format(len(pos_data_phrases_overlapped)))
#print(pos_data_phrases_overlap)
#ith open("Dataset_sample/pos_data_phrases_overlapped.csv", "w") as f:
#        write = csv.writer(f)
#        write.writerows(pos_data_phrases_overlapped)

#pos_data_phrases_unique = [x for x in pos_data_phrases_list2 if x[1] == 1]
#print("How many terminal noun phrases are unique? :{}".format(len(pos_data_phrases_unique))) 
#print(pos_data_phrases_unique)
#with open("Dataset_sample/pos_data_phrases_unique.csv", "w") as f:
#        write = csv.writer(f)
#        write.writerows(pos_data_phrases_unique)

gazette_data_phrases = [x.text for x in gazette_data_root.findall('sentences/sentence/entities/entity/ent_phrase')]
print("how many non-VPV terminal noun phrases do these TVDs have?:{}".format(len(gazette_data_phrases))) 
gazette_data_phrases_bins = collections.Counter(gazette_data_phrases).most_common()
print("how many distinct non-VPV terminal noun phrases?:{}".format(len(gazette_data_phrases_bins)))
#print(gazette_data_phrases_bins)

absolute_paths = [x.text for x in gazette_data_root.findall('sentences/sentence/entities/entity/absolute_path')]
absolute_paths_bins = collections.Counter(absolute_paths).most_common()
print("How many distinct absolute paths (those without index) are there for all non-VPV terminal noun phrases?:{}".format(len(absolute_paths_bins)))
for n in [2, 5, 10, 50, 100]:
        absolute_paths_repeated_n_times = [x for x in absolute_paths_bins if x[1] == n]
        print("How many distinct absolute paths (those without index) are repeated {} times for all non-VPV ?:{}".format(n, len(absolute_paths_repeated_n_times))) 
for n in [2, 5, 10, 50, 100]:
        absolute_paths_repeated_atleast_n_times = [x for x in absolute_paths_bins if x[1] >= n]
        print("How many distinct absolute paths (those without index) are repeated at least {} times for all non-VPV ?:{}".format(n, len(absolute_paths_repeated_atleast_n_times))) 

relative_paths = [x.text for x in gazette_data_root.findall('sentences/sentence/entities/entity/relative_paths/relative_path/path')]
relative_paths_bins = collections.Counter(relative_paths).most_common()
print("How many distinct relative paths (those without index) are there for all non-VPV terminal noun phrases?:{}".format(len(relative_paths_bins)))
for n in [2, 5, 10, 50, 100]:
        absolute_paths_repeated_n_times = [x for x in relative_paths_bins if x[1] == n]
        print("How many distinct relative paths (those without index) are repeated {} times for all non-VPV ?:{}".format(n, len(absolute_paths_repeated_n_times))) 
for n in [2, 5, 10, 50, 100]:
        absolute_paths_repeated_atleast_n_times = [x for x in relative_paths_bins if x[1] >= n]
        print("How many distinct relative paths (those without index) are repeated at least {} times for all non-VPV ?:{}".format(n, len(absolute_paths_repeated_atleast_n_times))) 
