# -*- coding: utf-8 -*-
"""
Created on Sun Mar  1 15:36:59 2020

@author: User
"""

#XML file 
import numpy as np
import collections
import json
import constants
from keras.models import model_from_json
from xml.etree import ElementTree
from interfaces import Input,InputPresenter, Encoder
from csv import reader
class EmbeddedPathEncoder(Encoder):
    def __init__(self, pathEmbeddingModelPath, optimizer, loss, metrics,pathEmbeddingDictionaryPath):
        self._optimizer=optimizer
        self._loss=loss
        self._metrics=metrics
        
        #Loading embedding models from files
        self._pathEmbeddingModel = self._loadModel(pathEmbeddingModelPath)
        
        #Loading path dictionaries
        pathDictionaryTuple,_ = self._loadDictionary(pathEmbeddingDictionaryPath)
        self._pathDictionary,_ = pathDictionaryTuple
        
    def _loadModel(self, path):
        file = open(path+".json", "r")
        model_json = file.read()
        model = model_from_json(model_json)
        model.load_weights(path+".h5")
        model.compile(optimizer=self._optimizer, loss=self._loss, metrics=[self._metrics])
        model.summary()
        return model
    
    def _loadDictionary(self, path):
        with open(path, 'r') as jsonFile:
            jsonDictionary = json.load(jsonFile)
        return tuple(json.loads(jsonDictionary))
        
    def encode(self, vocabulary):
        ordinals=[self._encodeSingle(i, vocabulary) for i in range(len(vocabulary))]
        sparseDimension = len(self._pathDictionary)
        unitVectors = np.eye(sparseDimension)
        oneHotVectors = unitVectors[ordinals]
        embeddedVectors = self._pathEmbeddingModel.predict(oneHotVectors)
        return {vocabulary[i][0]:embeddedVectors[i] for i in range(len(vocabulary))}, None
   
    def _encodeSingle(self, i, vocabulary):
       try:
           return self._pathDictionary[vocabulary[i][0]]
       except:
           return self._pathDictionary[constants.UNKNOWN]
class PathEncoder(Encoder):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        
    def encode(self, vocabulary):
        dictionary = {vocabulary[i][0]:i for i in range(len(vocabulary))}
        reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
        return dictionary, reverse_dictionary

class AutoLableInput(Input):
    def __init__(self, corpus,
             partitionRatio, absolutePathPresenterFactory, relativePathPresenterFactory,
             isAbsolutePath = True,
             pathEncoder= PathEncoder(), 
             relativePathEncoder = None,
             pathVocabularySize = constants.PATH_VOCABULARY_SIZE,
             **kwargs):
        self._sentences = corpus
        self._partitionRatio = partitionRatio
        self._absolutePathPresenterFactory = absolutePathPresenterFactory
        self._relativePathPresenterFactory = relativePathPresenterFactory
        self._isAbsolutePath = isAbsolutePath
        self._pathEncoder = pathEncoder
        if relativePathEncoder is None:
            self._relativePathEncoder = pathEncoder
        else:
            self._relativePathEncoder = relativePathEncoder
        self._pathVocabularySize = pathVocabularySize
       
            
        super().__init__(**kwargs)
    
#    def _calculatePathSequenceLength(self, ):
#        self._maxWordLength = max([len(word) for sequence in self._wordSequences for word in self._rawSequences])
#        self._maxSequenceLength = max([len(sequence) for sequence in self._wordSequences])


    def _buildDictionary_(self, elementFrequencies, encoder):
        
        #Our vocabulary is, including Unknown Words and Padding
        elementVocabulary = [[constants.PADDING, 1],[constants.UNKNOWN, 1]]
        elementVocabulary.extend([[element,frequence] for element,frequence in elementFrequencies])
        
        #obtain dictionary and riverse dictionary mapping words from our vocabulary 
        #to tokens
        return encoder.encode(elementVocabulary)

  
    
    def initializeEmptyPathInput(self):
        print("q###########################################################################")
        self.buildPathEmbeddingDictionaries()
        
        # Convert dictionaries to tuple ((wordDictionary, wordReverseDictionary),
        #   (labelDictionary, lebelReverseDictionary),(characterDictionary, characterReverseDictionary))
        self.encodeCorpus()
        self.reset()
        
    def initializeWithSavedDataAndDictionaryPathInput(self, path):
        self.load(path)
        self.encodeCorpus()
       
    def initializeWithDictionary(self, path, maxWordLength = None, maxSentenceLength = None):
        dictList = self._loadDictionary_(path+constants.INPUT_DICTIONARY)
        self._dictionaries = tuple(dictList);
            
        self.encodeCorpus()
        self.reset()

     
    def encodeCorpus(self):
#          def _encodeSequences_(self, sequences, encoder, dictionary):        
        encodedSentences = []
        absoluteDictionaries,relativeDictionaries = self._dictionaries
        absoluteDictionary,_=absoluteDictionaries
        relativeDictionary,_=relativeDictionaries
        for sentence in self._sentences:
            encodedSentence = []
            for absolutePath,relativePaths in sentence:
                if absolutePath is None:
                    encodedAbsolutePath = absoluteDictionary[constants.PADDING]
                else:
                    try:
                        encodedAbsolutePath = absoluteDictionary[absolutePath]
                    except:
                        encodedAbsolutePath = absoluteDictionary[constants.UNKNOWN]
                    
                encodedPhrase = []
                for relativePath in relativePaths:
                    if relativePath is None:
                        encodedRelativePath = relativeDictionary[constants.PADDING]
                    else:
                        try:
                            encodedRelativePath = relativeDictionary[relativePath]
                        except:
                            encodedRelativePath = relativeDictionary[constants.UNKNOWN]
                    encodedPhrase.append(encodedRelativePath)
                encodedSentence.append((encodedAbsolutePath, encodedPhrase))
            encodedSentences.append(encodedSentence)    
        print('###############3',len(encodedSentences))
        self._encodedCorpus = encodedSentences
        
    
    def buildPathEmbeddingDictionaries(self):
        relativePaths = [path for sentence in self._sentences for phrase in sentence for path in phrase[1] if path is not None]    
        absolutePaths = [path for sentence in self._sentences for path,_ in sentence]
        print("###########################################################################")
        #All words and vocabulary of the input
        relativePathVocabulary = collections.Counter(relativePaths).most_common(self._pathVocabularySize-2)
        absolutePathVocabulary = collections.Counter(absolutePaths).most_common(self._pathVocabularySize-2)
        self._dictionaries = (self._buildDictionary_(absolutePathVocabulary, self._pathEncoder), self._buildDictionary_(relativePathVocabulary, self._relativePathEncoder))        
        
                       
    def reset(self):
        #Random indexes for training, testing and validation datasets
        #Note individual piece of data is now a sequence       
        print('qweeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeea')
        bins = np.random.choice(3, len(self._encodedCorpus), p=list(self._partitionRatio))
        self._trainingData, = np.where(bins==0)
        self._validationData, = np.where(bins==1)
        self._testData, = np.where(bins==2)
        s = 0
        for i in range(len(self._encodedCorpus)):
            if i in self._trainingData:
                for j in self._encodedCorpus[i]:
                    s+=1
        for i in range(len(self._encodedCorpus)):
            if i in self._validationData:
                for j in self._encodedCorpus[i]:
                    s+=1
        for i in range(len(self._encodedCorpus)):
            if i in self._testData:
                for j in self._encodedCorpus[i]:
                    s+=1                    
        print(s)

        
    def getTrainingData(self):
        return self._absolutePathPresenterFactory(self._trainingData, self, lambda x: self._encodedCorpus[x]) \
            if self._isAbsolutePath \
            else self._relativePathPresenterFactory(self._trainingData, self, lambda x: self._encodedCorpus[x])
    
    def getTestData(self):
        return self._absolutePathPresenterFactory(self._testData, self, lambda x: self._encodedCorpus[x]) \
            if self._isAbsolutePath \
            else self._relativePathPresenterFactory(self._testData, self, lambda x: self._encodedCorpus[x])
    
    def getValidationData(self):
        return self._absolutePathPresenterFactory(self._validationData, self, lambda x: self._encodedCorpus[x]) \
            if self._isAbsolutePath \
            else self._relativePathPresenterFactory(self._validationData, self, lambda x: self._encodedCorpus[x])
    
    def save(self, path):
        #save the dataset
        np.save(path+constants.TRAINING_DATASET, self._trainingData)
        np.save(path+constants.VALIDATION_DATASET, self._validationData)
        np.save(path+constants.TEST_DATASET, self._testData)
        
        #Save input and label dictionaries
        self._saveDictionary_(self._dictionaries, path+constants.INPUT_DICTIONARY)
        
    def _saveDictionary_(self, dictionary, fileWithPath):
        jsonDictionary = json.dumps(dictionary);
        with open(fileWithPath, 'w') as jsonFile:
            json.dump(jsonDictionary, jsonFile)
        
    def _saveVocabulary_(self, vocabularies, fileWithPath):
        jsonVocabularies = json.dumps(vocabularies);
        with open(fileWithPath, 'w') as jsonFile:
            json.dump(jsonVocabularies, jsonFile)
        
        
    def load(self, path):
        self._trainingData = np.load(path+constants.TRAINING_DATASET)
        self._validationData = np.load(path+constants.VALIDATION_DATASET)
        self._testData = np.load(path+constants.TEST_DATASET)
        self._dictionaries  = self._loadDictionary_(path+constants.INPUT_DICTIONARY)  
                
    def _loadDictionary_(self, fileWithPath):
        with open(fileWithPath, 'r') as jsonFile:
            jsonDictionary = json.load(jsonFile)
        return tuple(json.loads(jsonDictionary))
                    
    
    def getPhrases(self, codes):
        return [self._dictionaries[1][code] for code in codes]
        
    def getPhraseCodes(self, phrases):
        return [self._dictionaries[0][phrase] for phrase in phrases]
        
    def getPathDictionarySize(self):
        return len(self._dictionaries[0][0])

    def getRelativePathDictionarySize(self):
        return len(self._dictionaries[1][0])    
    
   
    def getMaximumOfDictionariesLength(self):
        return len(max(max(self._dictionaries[0][0], key = len), max(self._dictionaries[1][0], key = len)))
    
    def getMaxPathLength(self):
        return self._maxWordLength
    
    def getIntermediateFactors(self):
        interMediateFactors = int(self.getPathDictionarySize() / constants.SYNTAX_PATH_VECTOR_DIMENSION)
        factorization = [];
        for d in range(2, interMediateFactors-1):
            if(interMediateFactors % d == 0 ):
                factorization.append(d)                
        if(interMediateFactors > 1):
            factorization.append(interMediateFactors)
            
        return                  
        
    def isAbsolutePath(self):
        return self._isAbsolutePath;
        
class EmbeddedALPresenter(InputPresenter):
    def __init__(self, batchSize, sourceIndices, sourceAccessor):    
        assert(batchSize < constants.BUFFER_SIZE)
        
        # Counting howmany elements do we have across the sequences
        nElements = 0
        for index in sourceIndices:
            nElements += len(sourceAccessor(index))
        self._nElements = nElements
        self._sourceIndices = sourceIndices
        self._sourceAccessor = sourceAccessor
        self._currentSequence = 0
        self._buffer = []
        self.__refill__()
        
        super().__init__(batchSize)
        
    def _fetch_(self):
        if self._batchSize > len(self._buffer):
            #refill the buffer
            self.__refill__()
        batch = []
        if self._batchSize > len(self._buffer):
            #this must be the last batch 
            batch = self._buffer
            self._currentSequence = 0
            self._buffer = []
            self.__refill__() #preparing for the next epoch
        else:
            batch = self._buffer[:self._batchSize]
            #remove the extracted batch from the buffer
            self._buffer = self._buffer[self._batchSize:]
        x = []
        for absolutePath, relativePath in batch:
            inputVector = [absolutePath];
            inputVector.extend(relativePath)
            inputVector = np.array(inputVector)
            currentShape = inputVector.shape
            newShape = (currentShape[0]*currentShape[1],)
            inputVector = inputVector.reshape(newShape)
            x.append(inputVector)
        

        return (np.array(x))
       
    def nBatches(self):
        return self._nElements//self._batchSize + 1

    def __refill__(self):
        while len(self._buffer) < constants.BUFFER_SIZE:
            if(self._currentSequence < len(self._sourceIndices)):
                sourceIndex = self._sourceIndices[self._currentSequence]
                sequence = self._sourceAccessor(sourceIndex)
                self._buffer.extend(sequence)
                self._currentSequence += 1
            else:
                break

def EmbeddedALPresenterFactory(indices, sequenceInput, sequencesAccessor):
    return EmbeddedALPresenter(constants.BATCH_SIZE, indices, sequencesAccessor)
           
class AbsolutePathPresenter(InputPresenter):
    def __init__(self, batchSize, sourceIndices, sourceAccessor, dictionarySize, **kwargs):
        assert(batchSize < constants.BUFFER_SIZE)
        
        # Counting howmany elements do we have across the sequences
        nElements = 0
        for index in sourceIndices:
            nElements += len(sourceAccessor(index))
        self._nElements = nElements
        self._sourceIndices = sourceIndices
        self._sourceAccessor = sourceAccessor
        self._dictionarySize = dictionarySize
        self._currentSequence = 0
        self._buffer = []
        self.__refill__()
        
        super().__init__(batchSize, **kwargs)
        
    def _fetch_(self):
        unitVectors = np.eye(self._dictionarySize)
        if self._batchSize > len(self._buffer):
            #refill the buffer
            self.__refill__()
        batch = []
        if self._batchSize > len(self._buffer):
            #this must be the last batch 
            batch = self._buffer
            self._currentSequence = 0
            self._buffer = []
            self.__refill__() #preparing for the next epoch
        else:
            batch = self._buffer[:self._batchSize]
            #remove the extracted batch from the buffer
            self._buffer = self._buffer[self._batchSize:]
        x = []
        for absolutePath, _ in batch:
            x.append(unitVectors[absolutePath])
        

        return (np.array(x),np.random.rand(len(x)))
       
    def nBatches(self):
        return self._nElements//self._batchSize + 1

    def __refill__(self):
        while len(self._buffer) < constants.BUFFER_SIZE:
            if(self._currentSequence < len(self._sourceIndices)):
                sourceIndex = self._sourceIndices[self._currentSequence]
                sequence = self._sourceAccessor(sourceIndex)
                self._buffer.extend(sequence)
                self._currentSequence += 1
            else:
                break
            
def AbsolutePathPresenterFactory(indices, sequenceInput, sequencesAccessor):
    return AbsolutePathPresenter(constants.BATCH_SIZE, indices, sequencesAccessor, sequenceInput.getPathDictionarySize())

class RelativePathPresenter(InputPresenter):
    def __init__(self, batchSize, sourceIndices, sourceAccessor, dictionarySize, **kwargs):
        assert(batchSize < constants.BUFFER_SIZE)
        
        # Counting howmany elements do we have across the sequences
        nElements = 0
        for index in sourceIndices:
            nElements += len(sourceAccessor(index))
        self._nElements = nElements
        self._sourceIndices = sourceIndices
        self._sourceAccessor = sourceAccessor
        self._dictionarySize = dictionarySize
        self._currentSequence = 0
        self._buffer = []
        self.__refill__()
        
        super().__init__(batchSize, **kwargs)
        
    def _fetch_(self):
        unitVectors = np.eye(self._dictionarySize)
        if self._batchSize > len(self._buffer):
            #refill the buffer
            self.__refill__()
        batch = []
        if self._batchSize > len(self._buffer):
            #this must be the last batch 
            batch = self._buffer
            self._currentSequence = 0
            self._buffer = []
            self.__refill__() #preparing for the next epoch
        else:
            batch = self._buffer[:self._batchSize]
            #remove the extracted batch from the buffer
            self._buffer = self._buffer[self._batchSize:]
        x = []
        for _, relativePaths in batch:
            for relativePath in relativePaths:
                x.append(unitVectors[relativePath])
        

        return (np.array(x),np.random.rand(len(x)))
       
    def nBatches(self):
        return self._nElements//self._batchSize + 1

    def __refill__(self):
        while len(self._buffer) < constants.BUFFER_SIZE:
            if(self._currentSequence < len(self._sourceIndices)):
                sourceIndex = self._sourceIndices[self._currentSequence]
                sequence = self._sourceAccessor(sourceIndex)
                self._buffer.extend(sequence)
                self._currentSequence += 1
            else:
                break
            
def RelativePathPresenterFactory(indices, sequenceInput, sequencesAccessor):
    return RelativePathPresenter(int(constants.BATCH_SIZE/4), indices, sequencesAccessor, sequenceInput.getPathDictionarySize())

class AutoLabelerPresenter(InputPresenter):
    def __init__(self, batchSize, sourceIndices, sourceAccessor, dictionarySizes, **kwargs):
        assert(batchSize < constants.BUFFER_SIZE)
        
        # Counting howmany elements do we have across the sequences
        nElements = 0;
        for index in sourceIndices:
            nElements += len(sourceAccessor(index))
        self._nElements = nElements
        self._sourceIndices = sourceIndices
        self._sourceAccessor = sourceAccessor
        self._dictionarySizes = dictionarySizes
        self._currentSequence = 0
        self._buffer = []
        self.__refill__()
        
        super().__init__(batchSize, **kwargs)
        
    def _fetch_(self):
        absolueUnitVectors = np.eye(self._dictionarySizes[0])
        relativeUnitVectors = np.eye(self._dictionarySizes[1])
        
        if self._batchSize > len(self._buffer):
            #refill the buffer
            self.__refill__()
        batch = []
        if self._batchSize > len(self._buffer):
            #this must be the last batch 
            batch = self._buffer
            self._currentSequence = 0
            self._buffer = []
            self.__refill__() #preparing for the next epoch
        else:
            batch = self._buffer[:self._batchSize]
            #remove the extracted batch from the buffer
            self._buffer = self._buffer[self._batchSize:]
        xa = []
        xr = []
        for absolutePath, relativePaths in batch:
            xa.append(absolueUnitVectors[absolutePath])
            r = []
            for relativePath in relativePaths:
                r.append(relativeUnitVectors[relativePath])
            xr.append(np.array(r))        
            
        return [np.array(xa), np.array(xr)]
       
    def nBatches(self):
        return self._nElements//self._batchSize + 1

    def __refill__(self):
        while len(self._buffer) < constants.BUFFER_SIZE:
            if(self._currentSequence < len(self._sourceIndices)):
                sourceIndex = self._sourceIndices[self._currentSequence]
                sequence = self._sourceAccessor(sourceIndex)
                self._buffer.extend(sequence)
                self._currentSequence += 1
            else:
                break

def AutoLabelerPresenterFactory(indices, sequenceInput, sequenceAccessor):
    return AutoLabelerPresenter(constants.BATCH_SIZE, indices, sequenceAccessor, (sequenceInput.getPathDictionarySize(), sequenceInput.getRelativePathDictionarySize()))
        
    
class XMLPathFile:
  
    def __init__(self, path):
        #the parsing input xml root
        root = ElementTree.parse(path).getroot()
        self._corpus = []
           
        for sentence in root.findall('sentences/sentence'):
            entityList = []
            for entity in sentence.findall('entities/entity'):
                absolutePath = entity.find('absolute_path').text
                for relativePath in entity.findall('relative_paths/relative_path'):
                    pathTuple = [None, None, None, None]
                    gazetteId = int(relativePath.find('gazette_id').text)
                    currentPath = relativePath.find('path').text
                    pathTuple[gazetteId] = currentPath
                entityList.append((absolutePath, pathTuple))           
            self._corpus.append(entityList)                        
       
    def getCorpus(self):
        return self._corpus
    
##############################################################################################
# Code for labeled corpus
##############################################################################################    

class XMLLabeledPathFile:
    def __init__(self, path):
#        labels = self.loadLabels(labelsPath)
        root = ElementTree.parse(path).getroot()
        self._corpus = []        
        for sentence in root.findall('sentences/sentence'):
            sentenceId = sentence.find('id').text
            entityList = []
            for entity in sentence.findall('entities/entity'):
                absolutePath = entity.find('absolute_path').text
                orderNo = entity.attrib['OrderNo']
                for relativePath in entity.findall('relative_paths/relative_path'):
                    pathTuple = [None, None, None, None]
                    gazetteId = int(relativePath.find('gazette_id').text)
                    currentPath = relativePath.find('path').text
                    pathTuple[gazetteId] = currentPath

                    label = 0
                    
                entityList.append(((absolutePath, pathTuple),int(label)))           
            self._corpus.append(entityList)                        
       
    def getCorpus(self):
        return self._corpus
    
    def loadLabels(self, labelsPath):
        with open(labelsPath, 'r') as f:
            csvReader = reader(f)
            listOfLabels = list(csvReader)
        # create a dictionary with (SentenceId,OrderNo)
        dictionary = {(r[0],r[1]):r[3] for r in listOfLabels[1:]}
        return dictionary

    
class LabeledAutoLabelInut(AutoLableInput):
    # redefining the base class method
    def encodeCorpus(self):
        print('3123132131231313131')
        # def _encodeSequences_(self, sequences, encoder, dictionary):        
        encodedSentences = []
        absoluteDictionaries,relativeDictionaries = self._dictionaries
        absoluteDictionary,_=absoluteDictionaries
        relativeDictionary,_=relativeDictionaries
        a = 1
        for sentence in self._sentences:

            a+=1
            encodedSentence = []
            k = 1
            for (absolutePath,relativePaths),label in sentence:

                k+=1
                if absolutePath is None:
                    encodedAbsolutePath = absoluteDictionary[constants.PADDING]
                else:
                    try:
                        encodedAbsolutePath = absoluteDictionary[absolutePath]
                    except:
                        encodedAbsolutePath = absoluteDictionary[constants.UNKNOWN]
                    
                encodedPhrase = []
                for relativePath in relativePaths:
                    if relativePath is None:
                        encodedRelativePath = relativeDictionary[constants.PADDING]
                    else:
                        try:
                            encodedRelativePath = relativeDictionary[relativePath]
                        except:
                            encodedRelativePath = relativeDictionary[constants.UNKNOWN]
                    encodedPhrase.append(encodedRelativePath)
                encodedSentence.append(((encodedAbsolutePath, encodedPhrase),label))
            encodedSentences.append(encodedSentence)  
        s = 0
        for i in encodedSentences:
            for j in i:
                s+=1
            
        print(s)
        self._encodedCorpus = encodedSentences
    
    #redefining the base class method
    def buildPathEmbeddingDictionaries(self):
        print('!!!!!!!!!!!!!!!!!!!!!!!')
        relativePaths = [path for sentence in self._sentences for phrase in sentence for path in phrase[0][1] if path is not None]    
        absolutePaths = [path[0] for sentence in self._sentences for path,_ in sentence]
        print(len(relativePaths),len(absolutePaths))
        
        #All words and vocabulary of the input
        absolutePathFrequencies = collections.Counter(absolutePaths)
        relativePathFrequencies = collections.Counter(relativePaths)
        #save the frequencies for analysis
        self._saveVocabulary_([absolutePathFrequencies, relativePathFrequencies], 'Dataset/autolabeler_vocabularies.txt')
        relativePathVocabulary = relativePathFrequencies.most_common(self._pathVocabularySize-2)
        absolutePathVocabulary = absolutePathFrequencies.most_common(self._pathVocabularySize-2)

        self._dictionaries = (self._buildDictionary_(absolutePathVocabulary, self._pathEncoder), self._buildDictionary_(relativePathVocabulary, self._pathEncoder))         

class LabeledAutoLabelerPresenter(AutoLabelerPresenter):
    #redefining the base class method
    def _fetch_(self):
        absolueUnitVectors = np.eye(self._dictionarySizes[0])
        relativeUnitVectors = np.eye(self._dictionarySizes[1])
        
        if self._batchSize > len(self._buffer):
            #refill the buffer
            self.__refill__()
        batch = []
        if self._batchSize > len(self._buffer):
            #this must be the last batch 
            batch = self._buffer
            self._currentSequence = 0
            self._buffer = []
            self.__refill__() #preparing for the next epoch
        else:
            batch = self._buffer[:self._batchSize]
            #remove the extracted batch from the buffer
            self._buffer = self._buffer[self._batchSize:]
        xa = []
        xr = []
        y = []
        for (absolutePath, relativePaths), label in batch:
            xa.append(absolueUnitVectors[absolutePath])
            r = []
            for relativePath in relativePaths:
                r.append(relativeUnitVectors[relativePath])
            xr.append(np.array(r))        
            y.append(label)
            
        return [[np.array(xa), np.array(xr)],np.array(y)]

def LabeledAutoLabelerPresenterFactory(indices, sequenceInput, sequencesAccessor):
    return LabeledAutoLabelerPresenter(constants.BATCH_SIZE, indices, sequencesAccessor, (sequenceInput.getPathDictionarySize(), sequenceInput.getRelativePathDictionarySize()))

def getLabeledInput(path,labelPath, trainTestSplit = True, pathVocabularySize = constants.PATH_VOCABULARY_SIZE):
    
    corpus = XMLLabeledPathFile(path, labelPath).getCorpus()
    inData = LabeledAutoLabelInut(corpus, (.8,0,.2),LabeledAutoLabelerPresenterFactory, None, pathVocabularySize=pathVocabularySize)
    inData.initializeEmptyPathInput()
    absDictionarySize = inData.getPathDictionarySize()
    relDictionarySize = inData.getRelativePathDictionarySize()
    trainigPresenter = inData.getTrainingData()
    testPresenter = inData.getTestData()
    trBatches = trainigPresenter.nBatches()
    tBatches = testPresenter.nBatches()
    
    batchSise = constants.BATCH_SIZE
    rows = (trBatches+tBatches)*batchSise
    all_abs_inputs = np.zeros((rows,absDictionarySize))
    all_rel_inputs = np.zeros((rows, 4, relDictionarySize))
    all_output = np.zeros((rows,))
    offset = 0;
    for i in range(trBatches):
        [batchxa, batchxr], batchy = trainigPresenter.__next__()
        rows = batchxa.shape[0]
        all_abs_inputs[offset:offset+rows] = batchxa
        all_rel_inputs[offset:offset+rows] = batchxr
        all_output[offset:offset+rows] = batchy
        offset += rows
    cutoff = offset
    for i in range(tBatches):
        [batchxa, batchxr], batchy = testPresenter.__next__()
        rows = batchxa.shape[0]
        if rows == 0:
            break
        all_abs_inputs[offset:offset+rows] = batchxa
        all_rel_inputs[offset:offset+rows] = batchxr
        all_output[offset:offset+rows] = batchy
        offset += rows
    if trainTestSplit :
        x_train_abs = all_abs_inputs[0:cutoff]
        x_train_rel = all_rel_inputs[0:cutoff]
        x_train = [x_train_abs, x_train_rel]
        y_train = all_output[0:cutoff]
        x_test_abs = all_abs_inputs[cutoff:]
        x_test_rel = all_rel_inputs[cutoff:]
        x_test = [x_test_abs, x_test_rel]
        y_test = all_output[cutoff:]        
        return ((absDictionarySize,relDictionarySize),(x_train, y_train),(x_test,y_test))
    else:
        x_train = [all_abs_inputs, all_rel_inputs]
        y_train = all_output
        return ((absDictionarySize,relDictionarySize),(x_train,y_train))

def main():
    path = 'Dataset/gazette_data_set_relative_path_noun_phrases_sample.xml'
    corpus = XMLPathFile(path).getCorpus()     
    input = AutoLableInput(corpus,(.8, .1, .1), AutoLabelerPresenterFactory,None)
    input.initializeEmptyPathInput() 
    presenter = input.getTrainingData()
    batch = presenter.__next__()
    print(len(batch))
    
if __name__ == '__main__':
    main()   