
#This works for Dataset_sample
'''
pos_data_base_file_name = 'Dataset_sample/pos_data_set_tuned_tagged_corpus_noun_phrases'
pos_data_file_name = pos_data_base_file_name + '.xml'
pos_data_fixed_file_name = pos_data_base_file_name + '_fixed.xml'
pos_data_orderid_file_name = pos_data_base_file_name + '_orderid.xml'

pos_data_phrases_overlapped_file_name = 'Dataset_sample/pos_data_phrases_overlapped.csv'
pos_data_phrases_unique_file_name = 'Dataset_sample/pos_data_phrases_unique.csv'

gazette_data_file_name = 'Dataset_sample/gazette_data_set_relative_path_noun_phrases.xml'
'''

#This works for both files suffic _sampled

pos_data_base_file_name = 'Dataset_sample/pos_data_set_tuned_tagged_corpus_noun_phrases_sample_orderid_sampled'
pos_data_file_name = pos_data_base_file_name + '.xml'
pos_data_fixed_file_name = pos_data_base_file_name + '_fixed.xml'
pos_data_orderid_file_name = pos_data_base_file_name + '_orderid.xml'

pos_data_phrases_overlapped_file_name = 'Dataset_sample/pos_data_phrases_overlapped.csv'
pos_data_phrases_unique_file_name = 'Dataset_sample/pos_data_phrases_unique.csv'

gazette_data_file_name = 'Dataset_sample/gazette_data_set_relative_path_noun_phrases_sampled.xml'


'''
#This works for main Dataset
pos_data_base_file_name = 'Dataset/pos_data_set_tuned_tagged_corpus_noun_phrases'
pos_data_file_name = pos_data_base_file_name + '.xml'
pos_data_fixed_file_name = pos_data_base_file_name + '_fixed.xml'
pos_data_orderid_file_name = pos_data_base_file_name + '_orderid.xml'

pos_data_phrases_overlapped_file_name = 'Dataset/pos_data_phrases_overlapped.csv'
pos_data_phrases_unique_file_name = 'Dataset/pos_data_phrases_unique.csv'

gazette_data_file_name = 'Dataset/gazette_data_set_relative_path_noun_phrases.xml'

'''

