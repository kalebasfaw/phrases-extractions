# -*- coding: utf-8 -*-
"""
Created on Wed Jan  6 06:32:03 2021

@author: 
"""

import numpy as np
import matplotlib.pyplot as plt

import tensorflow as tf
from tensorflow.keras.layers import Input, Dense, Lambda
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras import backend as K
from tensorflow.keras import losses
from tensorflow.keras.activations import softmax
from tensorflow.keras.losses import binary_crossentropy as bce

batch_size = 100
data_dim = 1000 
intermediate_dim = 500
latent_dim = 200

M = 3
N = 50
nb_epoch = 100
epsilon_std = 0.01

anneal_rate = 0.0003
min_temprature = 0.5

tau = K.variable(5.0, name='temprature')
x = Input(batch_shape=(batch_size, data_dim))
intermediate_layer = Dense(intermediate_dim, activation='relu')
latent_layer = Dense(latent_dim, activation='relu') 
logits_y_layer = Dense(M*N)

def sampling(logits_y):
    U = K.random_uniform(K.shape(logits_y), 0, 1)
    y = logits_y - K.log(-K.log(U +1e-20) + 1e-20)
    y = K.reshape(y, (-1,N,M))#/tau
    y = K.softmax(y)
    y = K.reshape(y, (-1, N*M))
    return y

intermediate = intermediate_layer(x)
latent = latent_layer(intermediate)
logits_y = logits_y_layer(latent)
encoder = Model(x, logits_y)

z = Lambda(sampling, output_shape=(M*N,))(logits_y)

latent_input = Input(shape=(M*N, ))
decoder_latent_layer = Dense(latent_dim, activation='relu')
decoder_intermediate_layer = Dense(intermediate_dim, activation='relu')
decoder_output_layer = Dense(data_dim, activation='sigmoid')

decoded_latent = decoder_latent_layer(z)
decoded_intermediate = decoder_intermediate_layer(decoded_latent)
decoded_x = decoder_output_layer(decoded_intermediate)

q_y = K.reshape(logits_y, (-1, N, M))
q_y = K.softmax(q_y)
log_q_y = K.log(q_y + 1e-20)
kl_tmp = q_y * (log_q_y - K.log(1.0/M))
KL = K.sum(kl_tmp, axis=(1,2))
elbo = data_dim * bce(x, decoded_x) - KL


vae = Model(x, decoded_x)
vae.add_loss(elbo)
vae.compile(optimizer='adam')
print(vae.summary())

import mixedModeRandomData as mmr
absDictionarySize = 1000
relDictionarySize = 1000

([x_train,_], y_train),([x_test,_], y_test) = mmr.generateMixedBinomDistributionData(absDictionarySize=absDictionarySize, relDictionarySize=relDictionarySize, sampleSize=10000, noiseSize=0)

for e in range(nb_epoch):
    vae.fit(x_train, x_train, shuffle=True, epochs=1, batch_size=batch_size,
            validation_data=(x_test, x_test))
    K.set_value(tau, np.max([K.get_value(tau) * np.exp(-anneal_rate*e), min_temprature]))
    
encoded_test = encoder.predict(x_test)    

from sklearn.manifold import TSNE
tsne = TSNE(metric='hamming')
viz = tsne.fit_transform(encoded_test)

plt.figure(figsize=(6,6))
plt.scatter(viz[:,0], viz[:, 1],c=y_test)
plt.colorbar()
plt.show()