
import xml.etree.ElementTree as et
import collections
import csv
import settings
pos_data_doc = et.parse(settings.pos_data_orderid_file_name)
pos_data_root = pos_data_doc.getroot()

gazette_data_doc = et.parse(settings.gazette_data_file_name)
gazette_data_root = gazette_data_doc.getroot()

f_sentence_phrases_unique = open("Dataset_sample/sentence_phrases_unique.csv", "w")
w_sentence_phrases_unique = csv.writer(f_sentence_phrases_unique)
for sentence in gazette_data_root.findall('sentences/sentence'): 
        sentence_id = sentence.find('id').text
        sentence_phrases = [x.text for x in sentence.findall('entities/entity/ent_phrase')]
        sentence_phrases_bins = collections.Counter(sentence_phrases).most_common()
        #print("How many distinct phrases per sentence? id: {}, count: {}".format(sentence_id, len(sentence_phrases_bins)))
        sentence_phrases_unique = [x[0] for x in sentence_phrases_bins if x[1] == 1]
        if len(sentence_phrases_unique):
                str = (sentence_id,len(sentence_phrases_unique),sentence_phrases_unique)
                #print("How many unique phrases per sentence? {} ".format(str))
                w_sentence_phrases_unique.writerow(str)
f_sentence_phrases_unique.close()

gazette_data_phrases = [x.text for x in gazette_data_root.findall('sentences/sentence/entities/entity/ent_phrase')]
gazette_data_phrases_bins = collections.Counter(gazette_data_phrases).most_common()
gazette_data_phrases_distinct = [x[0] for x in gazette_data_phrases_bins]
print("how many distinct phrases?:{}".format(len(gazette_data_phrases_distinct)))

f_distinct_phrase_absolute_paths = open("Dataset_sample/distinct_phrase_absolute_paths.csv", "w")
w_distinct_phrase_absolute_paths = csv.writer(f_distinct_phrase_absolute_paths)
for distinct_phrase in gazette_data_phrases_distinct:
        distinct_phrase_absolute_paths = []
        for entity in gazette_data_root.findall('sentences/sentence/entities/entity'): 
                if entity.find('id') is not None:
                        entity_id = entity.find('id').text
                else:
                        entity_id = -1
                
                if entity.find('ent_phrase') is not None:
                        entity_phrase = entity.find('ent_phrase').text
                else:
                        entity_phrase = 'unknown'
               
                entity_absolute_paths = [x.text for x in entity.findall('absolute_path') if x is not None]
                if distinct_phrase == entity_phrase:
                        for p in entity_absolute_paths:
                                distinct_phrase_absolute_paths.append(p)
        if len(distinct_phrase_absolute_paths):
                abs_str = (distinct_phrase,len(distinct_phrase_absolute_paths),distinct_phrase_absolute_paths)
                #print("distinct_phrase_absolute_paths {}".format(abs_str))
                w_distinct_phrase_absolute_paths.writerow(abs_str)
f_distinct_phrase_absolute_paths.close()

f_distinct_phrase_relative_paths  = open("Dataset_sample/distinct_phrase_relative_paths .csv", "w")
w_distinct_phrase_relative_paths = csv.writer(f_distinct_phrase_relative_paths)
for distinct_phrase in gazette_data_phrases_distinct:
        distinct_phrase_relative_paths = []
        for entity in gazette_data_root.findall('sentences/sentence/entities/entity'): 
                if entity.find('id') is not None:
                        entity_id = entity.find('id').text
                else:
                        entity_id = -1
                
                if entity.find('ent_phrase') is not None:
                        entity_phrase = entity.find('ent_phrase').text
                else:
                        entity_phrase = 'unknown'
                entity_relative_paths = [x.text for x in entity.findall('relative_paths/relative_path/path') if x is not None]
                if distinct_phrase == entity_phrase:
                        for p in entity_relative_paths:
                                distinct_phrase_relative_paths.append(p)
        if len(distinct_phrase_relative_paths):
                rel_str = (distinct_phrase,len(distinct_phrase_relative_paths),distinct_phrase_relative_paths)
                #print("distinct_phrase_relative_paths {}".format(rel_str))
                w_distinct_phrase_relative_paths.writerow(rel_str)
f_distinct_phrase_relative_paths.close()