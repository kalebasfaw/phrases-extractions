
import xml.etree.ElementTree as et
doc = et.parse('Dataset/gazette_data_set_relative_path_noun_phrases.xml')
root = doc.getroot()

#TVDs
sentences = root.findall('sentences/sentence')
sentences_count  = len(list(sentences))
print("gazette_data_set_relative_path_noun_phrases.xml: sentenses count:{}".format(sentences_count)) 

sentences_gazette = root.findall('sentences/sentence/gazettes/gazette/gazette_id')
sentences_gazette_count  = len(list(sentences_gazette))
print("gazette_data_set_relative_path_noun_phrases.xml: sentences_gazette_ount:{}".format(sentences_gazette_count)) 

sentences_product_count = 0
sentences_vendor_count = 0
sentences_version_count = 0
sentences_update_count = 0
for g in sentences_gazette:
    if g.text == '0':
        sentences_vendor_count += 1
    if g.text == '1':
        sentences_product_count += 1
    if g.text == '2':
        sentences_version_count += 1
    if g.text == '2':
        sentences_update_count += 1

print("gazette_data_set_relative_path_noun_phrases.xml: sentences_vendor_count:{}". format(sentences_vendor_count)) 
print("gazette_data_set_relative_path_noun_phrases.xml: sentences_product_count:{}".format(sentences_product_count)) 
print("gazette_data_set_relative_path_noun_phrases.xml: sentences_version_count:{}".format(sentences_version_count)) 
print("gazette_data_set_relative_path_noun_phrases.xml: sentences_updates_count:{}".format(sentences_update_count)) 

#Noun Phrases
phrases = root.findall('sentences/sentence/entities/entity/ent_phrase')
phrases_count  = len(list(phrases))
print("gazette_data_set_relative_path_noun_phrases.xml: phrases_count:{}".format(phrases_count)) 

phrases_gazette = root.findall('sentences/sentence/entities/entity/relative_paths/relative_path/gazette_id')
phrases_gazette_count  = len(list(phrases_gazette))
print("gazette_data_set_relative_path_noun_phrases.xml: phrases_gazette_count:{}".format(phrases_gazette_count)) 

phrases_vendor_count = 0
phrases_product_count = 0
phrases_version_count = 0
phrases_update_count = 0
for g in phrases_gazette:
    if g.text == '0':
        phrases_vendor_count += 1
    if g.text == '1':
        phrases_product_count += 1
    if g.text == '2':
        phrases_version_count += 1
    if g.text == '2':
        phrases_update_count += 1

print("gazette_data_set_relative_path_noun_phrases.xml: phrases_vendors_count:{}".format(phrases_vendor_count)) 
print("gazette_data_set_relative_path_noun_phrases.xml: phrases_products_count:{}".format(phrases_product_count)) 
print("gazette_data_set_relative_path_noun_phrases.xml: phrases_versions_count:{}".format(phrases_version_count)) 
print("gazette_data_set_relative_path_noun_phrases.xml: phrases_updatess_count:{}".format(phrases_update_count)) 
