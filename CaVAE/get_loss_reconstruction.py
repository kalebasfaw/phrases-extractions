#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 08:56:52 2021

@author: 
"""

def evaluate(path, labelPath):
    """    
    Trains the AutoLabelerGveaModel with the provided training data
    Parameters
    ----------
    path : TYPE. string TYPE.
        DESCRIPTION.
    Spefify the path to the xml file containing the selected for training noun-phrases 
        DESCRIPTION.
    labelPath: TYPE. string TYPE.
        DESCRIPTION.
    specify the path to the csv file containing noun-phrase to label mapping
    Returns
    -------
    None.

    """
    intermediate_dim = 599
    latent_dim = 200
    M = 3
    N = 50
    #load test data
    corpus = XMLLabeledPathFile(path, labelPath).getCorpus()
    inData = LabeledAutoLabelInut(corpus, (1,0,0),LabeledAutoLabelerPresenterFactory, None, pathVocabularySize=1000)
    inData.initializeWithDictionary('Dataset/autolabeler_')
    absDictionarySize = inData.getPathDictionarySize()
    relDictionarySize = inData.getRelativePathDictionarySize()
    
    # deserialize encoder model from JSON
    autolabelerEncoderModelFileName = "ALEncoder_model"
    provider = AutoLabelerGvaeModel(absDictionarySize, relDictionarySize, 4, intermediate_dim, latent_dim, M, N )
    encoder = provider.getEncoder()

    _loadWeights(encoder, autolabelerEncoderModelFileName)   
    

    x_evaluation, y_evaluation = _getLabeledInput(inData, trainTestSplit=False)
    abs_encoded_evaluation, rel_encoded_evaluation = encoder.predict(x_evaluation)
    rel_encoded_evaluation = np.reshape(rel_encoded_evaluation, (-1,4*M*N))
    encoded_evaluation = np.hstack((abs_encoded_evaluation,rel_encoded_evaluation))
    
    from sklearn.manifold import TSNE
    tsne = TSNE(metric='hamming')
    viz = tsne.fit_transform(encoded_evaluation)
    
    plt.figure(figsize=(6,6))
    plt.scatter(viz[:,0], viz[:, 1],c=y_evaluation)
    plt.colorbar()
    plt.show()

def _getLabeledInput(inputDataContainer, trainTestSplit = True):
    absDictionarySize = inputDataContainer.getPathDictionarySize()
    relDictionarySize = inputDataContainer.getRelativePathDictionarySize()
    trainigPresenter = inputDataContainer.getTrainingData()
    testPresenter = inputDataContainer.getTestData()
    trBatches = trainigPresenter.nBatches()
    tBatches = testPresenter.nBatches()
    
    batchSise = constants.BATCH_SIZE
    rows = (trBatches+tBatches)*batchSise
    all_abs_inputs = np.zeros((rows,absDictionarySize))
    all_rel_inputs = np.zeros((rows, 4, relDictionarySize))
    all_output = np.zeros((rows,))
    offset = 0;
    for i in range(trBatches):
        [batchxa, batchxr], batchy = trainigPresenter.__next__()
        rows = batchxa.shape[0]
        all_abs_inputs[offset:offset+rows] = batchxa
        all_rel_inputs[offset:offset+rows] = batchxr
        all_output[offset:offset+rows] = batchy
        offset += rows
    cutoff = offset
    for i in range(tBatches):
        [batchxa, batchxr], batchy = testPresenter.__next__()
        rows = batchxa.shape[0]
        if rows == 0:
            break
        all_abs_inputs[offset:offset+rows] = batchxa
        all_rel_inputs[offset:offset+rows] = batchxr
        all_output[offset:offset+rows] = batchy
        offset += rows
    if trainTestSplit :
        x_train_abs = all_abs_inputs[0:cutoff]
        x_train_rel = all_rel_inputs[0:cutoff]
        x_train = [x_train_abs, x_train_rel]
        y_train = all_output[0:cutoff]
        x_test_abs = all_abs_inputs[cutoff:]
        x_test_rel = all_rel_inputs[cutoff:]
        x_test = [x_test_abs, x_test_rel]
        y_test = all_output[cutoff:]        
        return ((x_train, y_train),(x_test,y_test))
    else:
        x_train = [all_abs_inputs, all_rel_inputs]
        y_train = all_output
        return (x_train,y_train)

def _saveModelWeights(model, fileName):
    # serialize weights to HDF5
    model.save_weights("TrainedModels/"+fileName+".h5")

def _loadWeights(model, fileName):
    model.load_weights("TrainedModels/"+fileName+".h5")





import numpy as np
import matplotlib.pyplot as plt

from autoLabelerInput import XMLLabeledPathFile, LabeledAutoLabelInut, LabeledAutoLabelerPresenterFactory
from AutoLablerGveaModel import AutoLabelerGvaeModel
import tsne_dbscan_rf as tdr
import sys; sys.path.append('/home/hanlinyi/下载/VAE_Code/FIt-SNE')
import constants
import sklearn

path = 'Dataset/gazette_data_set_relative_path_noun_phrases_NotLabled_4.xml'
labelPath = 'Dataset/20201129_LabledEntities.csv'
intermediate_dim = 599
latent_dim = 200
M = 3
N = 50

epochs = 45000
batch_size = constants.BATCH_SIZE

corpus = XMLLabeledPathFile(path, labelPath).getCorpus()
inData = LabeledAutoLabelInut(corpus, (.8,0,.2),LabeledAutoLabelerPresenterFactory, None, pathVocabularySize=1000)
inData.initializeEmptyPathInput()

(x_train, _), (x_test, y_test) = _getLabeledInput(inData)

absDictionarySize = inData.getPathDictionarySize()
relDictionarySize = inData.getRelativePathDictionarySize()

# train the auto-labeler
provider = AutoLabelerGvaeModel(absDictionarySize, relDictionarySize, 4, intermediate_dim, latent_dim, M, N )
model = provider.getModel()
encoder = provider.getEncoder()
loss = []
val_loss = []
epoach = []
temperature = []
BATCH_SIZE=100
NUM_ITERS=50000
tau0=1.0 # initial temperature
np_temp=tau0
np_lr=0.001
ANNEAL_RATE=0.00003
MIN_TEMP=0.5
import pandas as pd
data = pd.DataFrame()
#    dat.append([i,np_temp,np_loss])

for e in range(epochs):
    a = model.fit(x_train, x_train, shuffle=True, epochs=1, batch_size=batch_size,
            validation_data=(x_test, x_test))
    provider.updateTau(e)
    if e % 10 == 0:
        loss.append(a.history['loss'][0])
        val_loss.append(a.history['val_loss'][0])
        epoach.append(e)
        temperature.append(np_temp)
        data = pd.DataFrame()
        data['loss'] = loss
        data['val_loss'] = val_loss
        data['temperature'] = temperature
        data['epoach'] = epoach
#        data.to_csv('/home/hanlinyi/下载/VAE_Code/vae_loss.csv')
        try:
            data.to_csv('./vae_loss.csv',index = None)
        except Exception:
            pass

    if e % 100 == 0:
        np_temp=np.maximum(tau0*np.exp(-ANNEAL_RATE*e),MIN_TEMP)
        np_lr*=0.9
        







































