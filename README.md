**This project has a pipeline of models to extract phrase-based concepts in vulnerability descriptions through unsupervised labeling. 
Our project has the following main modules.**

**A WebScraper tool.** This module allows you to crawl the JSON and XML feeds of unstructured vulnerability reports from the national vulnerability database (NVD) website. 
The tool heuristically detects variations of vulnerability reports in the NVD and automatically crawls and builds an unannotated corpus of TVDs.

**Part-of-speech tagging (POS).** This module allows you to assign POS tags to tokens in TVDs. 
Our POS tagger allows you to use pretrained word embeddings (e.g., our unsupervised word embeddings), which can be trained with combined texts, such as natural language and domain-specific texts, including our unstructured vulnerability reports. 

**Unsupervised Concept Labeling (CaVAE).** This module allows you to train path representations of phrases via encoder-decoder architecture, cluster, and label the cluster with the concept type (e.g., vulnerability type, vulnerable component, root cause, attacker type, impact, or attack vector) that the majority of phrases represent.
The CaVAE is extended VAE with the Gumbel-Max trick, which suits modeling highly-sparse categorical data with encoder-decoder architecture.

**Concept Extraction.** 
This module allows you to fine-tune two models (e.g., concept classification, sequence labeling) for identifying and categorizing phrase-based concepts mentions in TVDs based on Bidirectional Encoder Representations from Transformers (BERT).

**Requirements and Installation.**
Our project is based on Python 3.6+, Keras 2.2+, tensorflow-gpu 2.0+ because method signatures and type tips are pretty good. Then, in your favorite virtual environment, run:

*pip install -r requirements.txt* 

All you need to do is make raw TVD sentences, load a pre-trained model and use it. 
We will make the detailed usage of all models with examples. 


---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: 
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).