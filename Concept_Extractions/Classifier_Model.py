# -*- coding: utf-8 -*-
"""
Created on Tue Apr 27 20:36:39 2021

@author: 
"""

import os
import sys
import warnings
import pickle
import datetime
import tensorflow as tf 
import pandas as pd
import traceback
import time 
import json
import numpy as np 
from tensorflow import keras 
from tensorflow.keras import layers,Input
from tensorflow.keras.layers import Dense,LSTM,Bidirectional,Dropout,Embedding,BatchNormalization
import random
warnings.filterwarnings("ignore")
data_df = pd.read_csv(r'./Labeled_Concepts.csv')
#data_df = pd.read_csv('./Labeled_phrase.csv')
data_df = data_df.drop(['CVE-ID'],axis=1)
data_df=data_df.sample(frac=1).reset_index(drop=True)
data_df = data_df.dropna(axis=0, how='any')
data_df.drop_duplicates(subset='Phrase-Concepts',keep='first',inplace=True)
n = len(data_df.index.tolist())
train_df = data_df[:int(n*0.8)].reset_index(drop=True)
test_df = data_df[int(n*0.8):].reset_index(drop=True)

x_train = train_df['Phrase-Concepts'].values.tolist()
y_train = train_df['Label'].values.tolist()
x_test = test_df['Phrase-Concepts'].values.tolist()
y_test = test_df['Label'].values.tolist()
#
#fp=open(r'Gazetters\CPE_DICT_PRODUCT.txt')
#PRODUCT = fp.readlines()
#fp.close()
#fp=open(r'Gazetters\CPE_DICT_UPDATE.txt')
#UPDATE = fp.readlines()
#fp.close()
#fp=open(r'Gazetters\CPE_DICT_VENDOR.txt')
#VENDOR = fp.readlines()
#fp.close()
#fp=open(r'Gazetters\CPE_DICT_VERSION.txt')
#VERSION = fp.readlines()
#fp.close()
#product = [i[:-1] for i in PRODUCT]
#update = [i[:-1] for i in UPDATE]
#vendor = [i[:-1] for i in VENDOR]
#version = [i[:-1] for i in VERSION]
#k=0
#from collections import Counter
#count_train = Counter(y_train)
#count_test = Counter(y_test)
#random.shuffle(product)
#random.shuffle(update)
#random.shuffle(vendor)
#random.shuffle(version)
#train_number = 5000
#test_number = 500
#product_train = product[:train_number]
##update_train = update[:2000]
#vendor_train = vendor[:train_number]
#version_train = version[:int(train_number/2)] + update[:int(train_number/2)]
#if len(version_train)!=len(vendor_train):
#    version_train = version[:train_number]
#product_test = product[train_number:train_number+test_number]
##update_test = update[2000:2500]
#vendor_test = vendor[train_number:train_number+test_number]
#version_test = version[train_number:train_number+int(test_number/2)] + update[train_number:train_number+int(test_number/2)]
#if len(version_test)!=len(vendor_test):
#    version_test = version[train_number:train_number+test_number]
#x_train.extend(product_train)
##x_train.extend(update_train)
#x_train.extend(vendor_train)
#x_train.extend(version_train)
#y_train.extend([7]*train_number)
#y_train.extend([8]*train_number)
#y_train.extend([9]*train_number)
#x_test.extend(product_test)
##x_train.extend(update_train)
#x_test.extend(vendor_test)
#x_test.extend(version_test)
#y_test.extend([7]*test_number)
##x_train.extend(update_train)
#y_test.extend([8]*test_number)
#y_test.extend([9]*test_number)
text_x_train = x_train
text_y_train = y_train
text_x_test = x_test
text_y_test = y_test

a = pd.DataFrame()
a['x'] = x_train
a['y'] = y_train
a = a.sample(frac=1).reset_index(drop=True)
x_train = a['x'].tolist()
y_train = a['y'].tolist()

#for i in range(len(x_train)):
#    for j in PRODUCT:
#        if j[:-1] in x_train[i]:
#            k+=1
#            x_train[i] = x_train[i].replace(j[:-1],'PRODUCT')
#    for j in UPDATE:
#        if j[:-1] in x_train[i]:
#            k+=1
#            x_train[i] = x_train[i].replace(j[:-1],'UPDATE')        
#    for j in VENDOR:
#        if j[:-1] in x_train[i]:  
#            k+=1             
#            x_train[i] = x_train[i].replace(j[:-1],'VENDOR')        
#    for j in VERSION:
#        if j[:-1] in x_train[i]:
#            k+=1
#            x_train[i] = x_train[i].replace(j[:-1],'VERSION')        
        
        



def replace_abbreviations(text):
    texts = []
    for item in text:
        print(item)
        item = item.lower().replace("it's", "it is").replace("i'm", "i am").replace("he's", "he is").replace("she's",
                                                                                                             "she is") \
            .replace("we're", "we are").replace("they're", "they are").replace("you're", "you are").replace("that's",
                                                                                                            "that is") \
            .replace("this's", "this is").replace("can't", "can not").replace("don't", "do not").replace("doesn't",
                                                                                                         "does not") \
            .replace("we've", "we have").replace("i've", " i have").replace("isn't", "is not").replace("won't",
                                                                                                       "will not") \
            .replace("hasn't", "has not").replace("wasn't", "was not").replace("weren't", "were not").replace("let's",
                                                                                                              "let us") \
            .replace("didn't", "did not").replace("hadn't", "had not").replace("waht's", "what is").replace("couldn't",
                                                                                                            "could not") \
            .replace("you'll", "you will").replace("you've", "you have")

        item = item.replace("'s", "")
        texts.append(item)

    return texts

#删除标点符号及其它字符


def clear_review(text):
    texts = []
    for item in text:
        item = item.replace("<br /><br />", "")
        item = re.sub("[^a-zA-Z]", " ", item.lower())
        texts.append(" ".join(item.split()))
    return texts

#删除停用词　＋　词形还原


def stemed_words(text):
    stop_words = stopwords.words("english")
    lemma = WordNetLemmatizer()
    texts = []
    for item in text:
        words = [lemma.lemmatize(w, pos='v') for w in item.split() if w not in stop_words]
        texts.append(" ".join(words))
    return texts

#文本预处理


def preprocess(text):
#    text = replace_abbreviations(text)
#    text = clear_review(text)
#     text = stemed_words(text)

    return text



import re
# import matplotlib.pyplot as plt
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
train_texts = preprocess(x_train)
test_texts = preprocess(x_test)

max_features = 6000
#texts = train_texts
texts = train_texts + test_texts
#分词
tok = Tokenizer(num_words=max_features)
tok.fit_on_texts(train_texts)
#序列
list_tok = tok.texts_to_sequences(texts)

maxlen = 50

seq_tok = pad_sequences(list_tok, maxlen=maxlen)

x_train = seq_tok[:len(train_texts)]





def one_hot_encode(raw_y, num_classes):
    index = np.array(raw_y)
    class_cnt = num_classes #np.max(index) + 1 
    out = np.zeros((index.shape[0], class_cnt))
    out[np.arange(index.shape[0]), index] = 1
    return out 
def load_sample(fn, max_seq_len, word_dict, num_classes):
    text_df = pd.read_csv(fn)
    raw_y = []
    raw_x = []
    for i in range(len(text_df)):
        label = text_df['label'][i]
        raw_y.append(int(label))

        text = text_df['text'][i]
        text_len = len(text)
        x = np.zeros(max_seq_len, dtype = np.int32)
    if text_len <= max_seq_len:
          for i in range(text_len):
            x[i] = word_dict[text[i]]
    else:
          for i in range(text_len - max_seq_len, text_len):
            x[i - text_len + max_seq_len] = word_dict[text[i]]
    raw_x.append(x)

    all_x = np.array(raw_x)
    all_y = one_hot_encode(raw_y, num_classes)
    return all_x, all_y 
def batch_iter(x, y, batch_size = 16):
    data_len = len(x)
    num_batch = (data_len + batch_size - 1) // batch_size
    indices = np.random.permutation(np.arange(data_len))
    x_shuff = x[indices]
    y_shuff = y[indices]
    for i in range(num_batch):
        start_offset = i*batch_size 
        end_offset = min(start_offset + batch_size, data_len)
        yield i, num_batch, x_shuff[start_offset:end_offset], y_shuff[start_offset:end_offset]
class RnnAttentionLayer(layers.Layer):
  def __init__(self, attention_size, drop_rate):
    super().__init__()
    self.attention_size = attention_size
    self.dropout = Dropout(drop_rate, name = "rnn_attention_dropout")

  def build(self, input_shape):
    self.attention_w = self.add_weight(name = "atten_w", shape = (input_shape[-1], self.attention_size), initializer = tf.random_uniform_initializer(), dtype = "float32", trainable = True)
    self.attention_u = self.add_weight(name = "atten_u", shape = (self.attention_size,), initializer = tf.random_uniform_initializer(), dtype = "float32", trainable = True)
    self.attention_b = self.add_weight(name = "atten_b", shape = (self.attention_size,), initializer = tf.constant_initializer(0.1), dtype = "float32", trainable = True)    
    super().build(input_shape)

  def call(self, inputs, training):
    x = tf.tanh(tf.add(tf.tensordot(inputs, self.attention_w, axes = 1), self.attention_b))
    x = tf.tensordot(x, self.attention_u, axes = 1)
    x = tf.nn.softmax(x)
    weight_out = tf.multiply(tf.expand_dims(x, -1), inputs)
    final_out = tf.reduce_sum(weight_out, axis = 1) 
    drop_out = self.dropout(final_out, training = training)
    return drop_out
class RnnLayer(layers.Layer):
  def __init__(self, rnn_size, drop_rate):
    super().__init__()
    fwd_lstm = LSTM(rnn_size, return_sequences = True, go_backwards= False, dropout = drop_rate, name = "fwd_lstm")
    bwd_lstm = LSTM(rnn_size, return_sequences = True, go_backwards = True, dropout = drop_rate, name = "bwd_lstm")
    self.bilstm = Bidirectional(merge_mode = "concat", layer = fwd_lstm, backward_layer = bwd_lstm, name = "bilstm")
    #self.bilstm = Bidirectional(LSTM(rnn_size, activation= "relu", return_sequences = True, dropout = drop_rate))

  def call(self, inputs, training):
    outputs = self.bilstm(inputs, training = training)
    return outputs

class Model(tf.keras.Model):
  def __init__(self, num_classes, drop_rate, vocab_size, embedding_size, rnn_size, attention_size):
    super().__init__()
    self.embedding_layer = Embedding(vocab_size, embedding_size, embeddings_initializer = "uniform", name = "embeding_0")
    self.rnn_layer = RnnLayer(rnn_size, drop_rate)
    self.attention_layer = RnnAttentionLayer(attention_size, drop_rate)
    self.rnn_layer = RnnLayer(rnn_size//2, drop_rate)
    self.attention_layer = RnnAttentionLayer(attention_size, drop_rate)
    self.dense_layer = Dense(num_classes, activation = "softmax", kernel_regularizer=keras.regularizers.l2(0.001), name = "dense_1")

  def call(self, input_x, training):
    x = self.embedding_layer(input_x)
    x = self.rnn_layer(x, training = training)
    x = self.attention_layer(x, training = training)
    x = self.dense_layer(x)
    return x
def early_stop(patience=0, min_delta=0, monitor='val_loss'):
    '''
    使用early stop的方法，当loss不再下降时，停止训练
    :param patience: 当loss不再下降时继续训练的batch数量
    :param min_delta: loss的阈值，loss需要下降到该值以下
    :param monitor: 需要监视的指标，默认为loss
    :return:
    '''
    callbacks = [
        keras.callbacks.EarlyStopping(
            monitor=monitor,
            min_delta=min_delta,
            patience=patience,
            verbose=1
        )
    ]
    return callbacks
model = Model(len(set(y_train)), drop_rate = 0.05, vocab_size = 10000, 
              embedding_size = 256, rnn_size = 128, attention_size = 128)
model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['acc'])
callbacks = early_stop()
model.fit(x_train, np.array([i - 1 for i in y_train]), batch_size=32,epochs=20,validation_split=0.3,callbacks=callbacks)
def predict_classes(x):
        """Generate class predictions for the input samples.

        The input samples are processed batch by batch.

        # Arguments
            x: input data, as a Numpy array or list of Numpy arrays
                (if the model has multiple inputs).
            batch_size: integer.
            verbose: verbosity mode, 0 or 1.

        # Returns
            A numpy array of class predictions.
        """
        
        if x.shape[-1] > 1:
            return x.argmax(axis=-1)
        else:
            return (x > 0.5).astype('int32')
x_test = seq_tok[len(train_texts):]
y_pred = model.predict(x_test)
y_pred = predict_classes(y_pred)
model.save_weights('my_model.h5')
from sklearn.metrics import accuracy_score,f1_score,precision_score,recall_score

print('acc:',accuracy_score(y_test, [i+1 for i in y_pred]),' f1:', f1_score(y_test, [i+1 for i in y_pred], average='weighted' ),' precision:', precision_score(y_test, [i+1 for i in y_pred], average='weighted' ),' recall:', recall_score(y_test, [i+1 for i in y_pred], average='weighted' )   )

class_1 = ([i for i in y_test if i == 1 ],[y_pred[i] + 1 for i in range(len(y_test)) if y_test[i] == 1 ])
class_2 = ([i for i in y_test if i == 2 ],[y_pred[i] + 1 for i in range(len(y_test)) if y_test[i] == 2 ])
class_3 = ([i for i in y_test if i == 3 ],[y_pred[i] + 1 for i in range(len(y_test)) if y_test[i] == 3 ])
class_4 = ([i for i in y_test if i == 4 ],[y_pred[i] + 1 for i in range(len(y_test)) if y_test[i] == 4 ])
class_5 = ([i for i in y_test if i == 5 ],[y_pred[i] + 1 for i in range(len(y_test)) if y_test[i] == 5 ])
class_6 = ([i for i in y_test if i == 6 ],[y_pred[i] + 1 for i in range(len(y_test)) if y_test[i] == 6 ])
class_7 = ([i for i in y_test if i == 7 ],[y_pred[i] + 1 for i in range(len(y_test)) if y_test[i] == 7 ])

print('acc_class_1:',accuracy_score(class_1[0],class_1[1]),' f1_class_1:',f1_score(class_1[0],class_1[1], average='weighted'),' precision_class_1:',precision_score(class_1[0],class_1[1], average='weighted'),' recall_class_1:',recall_score(class_1[0],class_1[1], average='weighted'))
print('acc_class_2:',accuracy_score(class_2[0],class_2[1]),' f1_class_2:',f1_score(class_2[0],class_2[1], average='weighted'),' precision_class_2:',precision_score(class_2[0],class_2[1], average='weighted'),' recall_class_2:',recall_score(class_2[0],class_2[1], average='weighted'))
print('acc_class_3:',accuracy_score(class_3[0],class_3[1]),' f1_class_3:',f1_score(class_3[0],class_3[1], average='weighted'),' precision_class_3:',precision_score(class_3[0],class_3[1], average='weighted'),' recall_class_3:',recall_score(class_3[0],class_3[1], average='weighted'))
print('acc_class_4:',accuracy_score(class_4[0],class_4[1]),' f1_class_4:',f1_score(class_4[0],class_4[1], average='weighted'),' precision_class_4:',precision_score(class_4[0],class_4[1], average='weighted'),' recall_class_4:',recall_score(class_4[0],class_4[1], average='weighted'))
print('acc_class_5:',accuracy_score(class_5[0],class_5[1]),' f1_class_5:',f1_score(class_5[0],class_5[1], average='weighted'),' precision_class_5:',precision_score(class_5[0],class_5[1], average='weighted'),' recall_class_5:',recall_score(class_5[0],class_5[1], average='weighted'))
print('acc_class_6:',accuracy_score(class_6[0],class_6[1]),' f1_class_6:',f1_score(class_6[0],class_6[1], average='weighted'),' precision_class_6:',precision_score(class_6[0],class_6[1], average='weighted'),' recall_class_6:',recall_score(class_6[0],class_6[1], average='weighted'))
print('acc_class_7:',accuracy_score(class_7[0],class_7[1]),' f1_class_7:',f1_score(class_7[0],class_7[1], average='weighted'),' precision_class_7:',precision_score(class_7[0],class_7[1], average='weighted'),' recall_class_7:',recall_score(class_7[0],class_7[1], average='weighted'))




a = pd.DataFrame()
b = pd.DataFrame()
a['train_voc'] = text_x_train
a['train_label'] = text_y_train
b['test_voc'] = text_x_test
b['test_true_label'] = text_y_test
b['test_predict_label'] = [i+1 for i in y_pred]
a.to_csv('train_data.csv',index = None)
b.to_csv('predict_data.csv',index = None)




# from collections import Counter
# Counter(y_train)


import pandas as pd
data = pd.read_excel(r'./Descriptions.xlsx')
sentence = data['Descriptions'].tolist()
n_grame = 9


import spacy
nlp = spacy.load('en_core_web_sm')
sen_word = []
for i in sentence:
    temp = []
    doc = nlp(i)
    for token in doc:
        temp.append(token.text)
    sen_word.append(temp)
word_all = []
# for i in sen_word:
#     word_ngram = []
for j in range(1,n_grame+1):
    scout = 0
    word_ngram = []
    for i in sen_word:
        scout = 0
        while scout+j < len(i):
            word_ngram.append(i[scout:scout+j])
            scout += 1
    word_all.append(word_ngram)
a = []
for i in word_all:
    b = []
    for j in i:
        b.append(" ".join(j))
    a.append(b)
word_all = a    
list_tok_ngram_all = []
result_all = []
for i in word_all:
    list_tok_ngram = tok.texts_to_sequences(i)
    list_tok_ngram_all.append(list_tok_ngram)
    seq_tok_list_tok_ngram = pad_sequences(list_tok_ngram, maxlen=maxlen)
    result_pred = model.predict(seq_tok_list_tok_ngram)
    result_pred = predict_classes(result_pred)
    result_all.append(result_pred)
    

for i in range(len(result_all)):
    a = pd.DataFrame()
    a['word'] = word_all[i]
    a['label'] = result_all[i]
    a.to_csv('./'+str(i+1)+'-gram.csv',index = None)
    
    
temp = []
with open("Manually LabeledData.txt", "r") as f:
    for line in f.readlines():
            line = line.strip('\n')  #去掉列表中每一个元素的换行符
            temp.append(line)
   
list_tok_ngram_all = []
result_all = []    
i = temp
list_tok_ngram = tok.texts_to_sequences(i)
list_tok_ngram_all.append(list_tok_ngram)
seq_tok_list_tok_ngram = pad_sequences(list_tok_ngram, maxlen=maxlen)
result_pred = model.predict(seq_tok_list_tok_ngram)
result_pred = predict_classes(result_pred)
result_all.append(result_pred)    


a = pd.DataFrame()
a['word'] = temp
a['label'] = result_all[0]+1
a.to_csv('customer.csv',index = None)

label = a['label'].tolist()
aa = []
for i in label:
    if i == 1:
        aa.append('vulnerability type')
    if i == 2:
        aa.append('attack vector')
    if i == 3:
        aa.append('impact')    
    if i == 4:
        aa.append('vulnerable component')    
    if i == 5:
        aa.append('root casue')    
    if i == 6:
        aa.append('attacker type')    
    if i == 7:
        aa.append('O')    
a['label'] = aa
a.to_csv('customer.csv',index = None)  