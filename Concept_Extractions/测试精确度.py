# -*- coding: utf-8 -*-
"""
Created on Wed Dec  8 11:21:55 2021

@author: 
"""

with open(r'D:\ASE2021\bert_github\BERT-for-Sequence-Labeling-and-Text-Classification-master\output_model\snips_Slot_Filling_epochs3\slot_filling_test_results.txt','r') as f:
    ff = f.readlines()
with open(r'D:\ASE2021\bert_github\BERT-for-Sequence-Labeling-and-Text-Classification-master\data\snips_Intent_Detection_and_Slot_Filling\test\seq - 副本.out','r') as f:
    ff1 = f.readlines()
data_pre = [i.split(' ')[1:-1] for i in ff]
data_stand = [i.split(' ') for i in ff1]
#两个数据长度对齐
for i in range(len(data_stand)):
    if len(data_stand[i]) < len(data_pre[i]):
        data_pre[i] = data_pre[i][:len(data_stand[i])]
    if len(data_stand[i]) > len(data_pre[i]):
        data_stand[i] = data_stand[i][:len(data_pre[i])]
acc_stand = []
acc_pre = []        
for i in range(len(data_pre)):
    temp1 = data_pre[i]
    temp2 = data_stand[i]
    for j in range(len(temp1)):
        acc_stand.append(1)
        if temp1[j] == temp2[j]:
            acc_pre.append(1)
        else:
            acc_pre.append(0)
from sklearn.metrics import accuracy_score,f1_score,recall_score,precision_score
print(accuracy_score(acc_stand,acc_pre))
print(f1_score(acc_stand,acc_pre))
print(recall_score(acc_stand,acc_pre))
print(precision_score(acc_stand,acc_pre))


acc_vulnerability_stand = []
acc_vulnerability_pre = []   
for i in range(len(data_pre)):
    temp1 = data_pre[i]
    temp2 = data_stand[i]
    for j in range(len(temp1)):
        if 'vulnerability_type' in temp1[j] and 'vulnerability_type' in temp2[j]:
            acc_vulnerability_stand.append(1)
            if temp1[j] == temp2[j]:
                acc_vulnerability_pre.append(1)
            else:
                acc_vulnerability_pre.append(0)

acc_vulnerable_stand = []
acc_vulnerable_pre = []   
for i in range(len(data_pre)):
    temp1 = data_pre[i]
    temp2 = data_stand[i]
    for j in range(len(temp1)):
        if 'vulnerable_component' in temp1[j] and 'vulnerable_component' in temp2[j]:
            acc_vulnerable_stand.append(1)
            if temp1[j] == temp2[j]:
                acc_vulnerable_pre.append(1)
            else:
                acc_vulnerable_pre.append(0)

acc_root_cause_stand = []
acc_root_cause_pre = []   
for i in range(len(data_pre)):
    temp1 = data_pre[i]
    temp2 = data_stand[i]
    for j in range(len(temp1)):
        if 'root_cause' in temp1[j] and 'root_cause' in temp2[j]:
            acc_root_cause_stand.append(1)
            if temp1[j] == temp2[j]:
                acc_root_cause_pre.append(1)
            else:
                acc_root_cause_pre.append(0)

acc_attacker_type_stand = []
acc_attacker_type_pre = []   
for i in range(len(data_pre)):
    temp1 = data_pre[i]
    temp2 = data_stand[i]
    for j in range(len(temp1)):
        if 'attacker_type' in temp1[j] and 'attacker_type' in temp2[j]:
            acc_attacker_type_stand.append(1)
            if temp1[j] == temp2[j]:
                acc_attacker_type_pre.append(1)
            else:
                acc_attacker_type_pre.append(0)

acc_impact_stand = []
acc_impact_pre = []   
for i in range(len(data_pre)):
    temp1 = data_pre[i]
    temp2 = data_stand[i]
    for j in range(len(temp1)):
        if 'impact' in temp1[j] and 'impact' in temp2[j]:
            acc_impact_stand.append(1)
            if temp1[j] == temp2[j]:
                acc_impact_pre.append(1)
            else:
                acc_impact_pre.append(0)

acc_attacker_vector_stand = []
acc_attacker_vector_pre = []   
for i in range(len(data_pre)):
    temp1 = data_pre[i]
    temp2 = data_stand[i]
    for j in range(len(temp1)):
        if 'attacker_vector' in temp1[j] and 'attacker_vector' in temp2[j]:
            acc_attacker_vector_stand.append(1)
            if temp1[j] == temp2[j]:
                acc_attacker_vector_pre.append(1)
            else:
                acc_attacker_vector_pre.append(0)

acc_O_stand = []
acc_O_pre = []   
for i in range(len(data_pre)):
    temp1 = data_pre[i]
    temp2 = data_stand[i]
    for j in range(len(temp1)):
        if 'O' in temp1[j] and 'O' in temp2[j]:
            acc_O_stand.append(1)
            if temp1[j] == temp2[j]:
                acc_O_pre.append(1)
            else:
                acc_O_pre.append(0)

print('vulnerability:')
print(accuracy_score(acc_vulnerability_stand,acc_vulnerability_pre))
print(f1_score(acc_vulnerability_stand,acc_vulnerability_pre))
print(recall_score(acc_vulnerability_stand,acc_vulnerability_pre))
print(precision_score(acc_vulnerability_stand,acc_vulnerability_pre))

print('vulnerable:')
print(accuracy_score(acc_vulnerable_stand,acc_vulnerable_pre))
print(f1_score(acc_vulnerable_stand,acc_vulnerable_pre))
print(recall_score(acc_vulnerable_stand,acc_vulnerable_pre))
print(precision_score(acc_vulnerable_stand,acc_vulnerable_pre))

print('root_cause:')
print(accuracy_score(acc_root_cause_stand,acc_root_cause_pre))
print(f1_score(acc_root_cause_stand,acc_root_cause_pre))
print(recall_score(acc_root_cause_stand,acc_root_cause_pre))
print(precision_score(acc_root_cause_stand,acc_root_cause_pre))

print('attacker_type:')
print(accuracy_score(acc_attacker_type_stand,acc_attacker_type_pre))
print(f1_score(acc_attacker_type_stand,acc_attacker_type_pre))
print(recall_score(acc_attacker_type_stand,acc_attacker_type_pre))
print(precision_score(acc_attacker_type_stand,acc_attacker_type_pre))

print('impact:')
print(accuracy_score(acc_impact_stand,acc_impact_pre))
print(f1_score(acc_impact_stand,acc_impact_pre))
print(recall_score(acc_impact_stand,acc_impact_pre))
print(precision_score(acc_impact_stand,acc_impact_pre))

print('attacker_vector:')
print(accuracy_score(acc_attacker_vector_stand,acc_attacker_vector_pre))
print(f1_score(acc_attacker_vector_stand,acc_attacker_vector_pre))
print(recall_score(acc_attacker_vector_stand,acc_attacker_vector_pre))
print(precision_score(acc_attacker_vector_stand,acc_attacker_vector_pre))

print('O:')
print(accuracy_score(acc_O_stand,acc_O_pre))
print(f1_score(acc_O_stand,acc_O_pre))
print(recall_score(acc_O_stand,acc_O_pre))
print(precision_score(acc_O_stand,acc_O_pre))












