# -*- coding: utf-8 -*-
"""
Created on Wed Oct 20 16:41:34 2021

@author: 
"""

import pandas as pd
import numpy as np
def index_of_str(s1, s2):
    res = []
    len1 = len(s1)
    len2 = len(s2)
    if s1 == "" or s2 == "":
        return -1
    for i in range(len1 - len2 + 1):
        if s1[i] == s2[0] and s1[i:i+len2] == s2:
#            res.append(i)
            return i
#    return res if res else -1
with open(r'D:\ASE2021\111(3)(1).txt','r',encoding = 'utf8') as f:
    ff = f.read()
data = ff.split('--end--')
d = pd.read_csv(r'D:\ASE2021\bert_github\We_Use_This_data\Use_this_as_Input.csv')
vulnerability = [i if type(i)!=float else '' for i in d['vulnerability type'].tolist()]
attack = [i if type(i)!=float else '' for i in d['attacker vector'].tolist()]
impact = [i if type(i)!=float else '' for i in d['impact'].tolist()]
affected = [i if type(i)!=float else '' for i in d['affected product'].tolist()]
root = [i if type(i)!=float else '' for i in d['root cause'].tolist()]
attacker = [i if type(i)!=float else '' for i in d['attacker type'].tolist()]
s = 0
BIO = []
for i in data:
    s+=1
    print(s)
    loc = []
    for j in range(len(vulnerability)):
        if vulnerability[j] in i and attack[j] in i and impact[j] in i and affected[j] in i and root[j] in i and attacker[j] in i:
            if vulnerability[j] != '':
                l = index_of_str(i, vulnerability[j])
                L = l+len(vulnerability[j])
                token_number = vulnerability[j].count(' ') + 1
                left_token_number = i[:l].count(' ')
                loc.append([left_token_number, token_number])
            else:
                loc.append([])
            if attack[j] != '':
                l = index_of_str(i, attack[j])
                L = l+len(attack[j])
                token_number = attack[j].count(' ') + 1
                left_token_number = i[:l].count(' ')
                loc.append([left_token_number, token_number])
            else:
                loc.append([])
            if impact[j] != '':
                l = index_of_str(i, impact[j])
                L = l+len(impact[j])
                token_number = impact[j].count(' ') + 1
                left_token_number = i[:l].count(' ')
                loc.append([left_token_number, token_number])
            else:
                loc.append([])
            if affected[j] != '':
                l = index_of_str(i, affected[j])
                L = l+len(affected[j])
                token_number = affected[j].count(' ') + 1
                left_token_number = i[:l].count(' ')
                loc.append([left_token_number, token_number])
            else:
                loc.append([])
            if root[j] != '':
                l = index_of_str(i, root[j])
                L = l+len(root[j])
                token_number = root[j].count(' ') + 1
                left_token_number = i[:l].count(' ')
                loc.append([left_token_number, token_number])
            else:
                loc.append([])
            if attacker[j] != '':
                l = index_of_str(i, attacker[j])
                L = l+len(attacker[j])
                token_number = attacker[j].count(' ') + 1
                left_token_number = i[:l].count(' ')
                loc.append([left_token_number, token_number])
            else:
                loc.append([])
            break
    bio = ['O'] * (i.count(' ')+1)
    if loc == []:
        pass
    else:
        if loc[0]!=[]:
            bio[loc[0][0]] = 'B-vulnerability_type'
            for k in range(loc[0][0]+1, loc[0][0] + loc[0][1]):
                bio[k] = 'I-vulnerability_type'
        if loc[1]!=[]:
            bio[loc[1][0]] = 'B-attacker_vector'
            for k in range(loc[1][0]+1, loc[1][0] + loc[1][1]):
                bio[k] = 'I-attacker_vector'
        if loc[2]!=[]:
            bio[loc[2][0]] = 'B-impact'
            for k in range(loc[2][0]+1, loc[2][0] + loc[2][1]):
                bio[k] = 'I-impact'            
        if loc[3]!=[]:
            bio[loc[3][0]] = 'B-affected_product'
            for k in range(loc[3][0]+1, loc[3][0] + loc[3][1]):
                bio[k] = 'I-affected_product'        
        if loc[4]!=[]:
            bio[loc[4][0]] = 'B-root_cause'
            for k in range(loc[4][0]+1, loc[4][0] + loc[4][1]):
                bio[k] = 'I-root_cause'
        if loc[5]!=[]:
            bio[loc[5][0]] = 'B-attacker_type'
            for k in range(loc[5][0]+1, loc[5][0] + loc[5][1]):
                bio[k] = 'I-attacker_type'
    BIO.append(bio)
a = pd.DataFrame()
a['sentence'] = data
a['BIO'] = BIO
a[a.index==152439]['sentence'].tolist()
a= a.dropna()
a.to_csv(r'D:\ASE2021\bert_github\BIO标签数据.csv',index = None)





s = 0
BIO = []
for i in data:
    s+=1
    print(s)
    loc = [[],[],[],[],[],[]]
    for j in range(len(vulnerability)):
        if vulnerability[j] in i or attack[j] in i or impact[j] in i or affected[j] in i or root[j] in i or attacker[j] in i:
            if vulnerability[j] != '' and vulnerability[j] in i:
                l = index_of_str(i, vulnerability[j])
                L = l+len(vulnerability[j])
                token_number = vulnerability[j].count(' ') + 1
                left_token_number = i[:l].count(' ')
                loc[0] = [left_token_number, token_number]
            if attack[j] != '' and attack[j] in i:
                l = index_of_str(i, attack[j])
                L = l+len(attack[j])
                token_number = attack[j].count(' ') + 1
                left_token_number = i[:l].count(' ')
                loc[1] = [left_token_number, token_number]
            if impact[j] != '' and impact[j] in i:
                l = index_of_str(i, impact[j])
                L = l+len(impact[j])
                token_number = impact[j].count(' ') + 1
                left_token_number = i[:l].count(' ')
                loc[2] = [left_token_number, token_number]
            if affected[j] != '' and affected[j] in i:
                l = index_of_str(i, affected[j])
                L = l+len(affected[j])
                token_number = affected[j].count(' ') + 1
                left_token_number = i[:l].count(' ')
                loc[3] = [left_token_number, token_number]
            if root[j] != '' and root[j] in i:
                l = index_of_str(i, root[j])
                L = l+len(root[j])
                token_number = root[j].count(' ') + 1
                left_token_number = i[:l].count(' ')
                loc[4] = [left_token_number, token_number]
            if attacker[j] != '' and attacker[j] in i:
                l = index_of_str(i, attacker[j])
                L = l+len(attacker[j])
                token_number = attacker[j].count(' ') + 1
                left_token_number = i[:l].count(' ')
                loc[5] = [left_token_number, token_number]
    bio = ['O'] * (i.count(' ')+1)
    if loc == []:
        pass
    else:
        if loc[0]!=[]:
            bio[loc[0][0]] = 'B-vulnerability_type'
            for k in range(loc[0][0]+1, loc[0][0] + loc[0][1]):
                bio[k] = 'I-vulnerability_type'
        if loc[1]!=[]:
            bio[loc[1][0]] = 'B-attacker_vector'
            for k in range(loc[1][0]+1, loc[1][0] + loc[1][1]):
                bio[k] = 'I-attacker_vector'
        if loc[2]!=[]:
            bio[loc[2][0]] = 'B-impact'
            for k in range(loc[2][0]+1, loc[2][0] + loc[2][1]):
                bio[k] = 'I-impact'            
        if loc[3]!=[]:
            bio[loc[3][0]] = 'B-affected_product'
            for k in range(loc[3][0]+1, loc[3][0] + loc[3][1]):
                bio[k] = 'I-affected_product'        
        if loc[4]!=[]:
            bio[loc[4][0]] = 'B-root_cause'
            for k in range(loc[4][0]+1, loc[4][0] + loc[4][1]):
                bio[k] = 'I-root_cause'
        if loc[5]!=[]:
            bio[loc[5][0]] = 'B-attacker_type'
            for k in range(loc[5][0]+1, loc[5][0] + loc[5][1]):
                bio[k] = 'I-attacker_type'
    BIO.append(bio)
a = pd.DataFrame()
a['sentence'] = data
a['BIO'] = BIO
a.to_csv(r'D:\ASE2021\bert_github\BIO标签数据1.csv',index = None)


