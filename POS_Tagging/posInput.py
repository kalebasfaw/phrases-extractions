# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 19:07:49 2019

@author: Admin
"""
import nltk
import interfaces
import numpy as np
import constants
from keras.utils import np_utils

class TreebankInput:
    def __init__(self, path):
        if(path is not None):
            nltk.data.path.append(path)
        from nltk.corpus import treebank
        taggedSentences = treebank.tagged_sents()
        sentences = []
        sentenceTags = []
        for taggedSentence in taggedSentences:
            sentence, tags = zip(*taggedSentence)
            sentences.append(list(sentence))
            sentenceTags.append(list(tags))
        self._sentences = sentences
        self._sentenceTags = sentenceTags
        
    def getCorpus(self):
        return self._sentences, self._sentenceTags

class PosInputPresenter(interfaces.InputPresenter):
    def __init__(self, batchSize, sourceIndices, sourceAccesser,
                 tagDictionarySize, maxSequenceLength, maxWordLength, **kwargs):
        self._sourceIndices = sourceIndices
        self._sourceAccesser = sourceAccesser
        self._tagDictionarySize = tagDictionarySize
        self._maxSequenceLength = maxSequenceLength
        self._maxWordLength = maxWordLength
        
        self._currentSequence = 0
        self._buffer = []
        self.__refill__()
        
        super().__init__(batchSize, **kwargs)
        
        
    def _fetch_(self):
        if self._batchSize > len(self._buffer):
            #refill the buffer
            self.__refill__()
        batch = []
        if self._batchSize > len(self._buffer):
            #this must be the last batch 
            batch = self._buffer
            self._currentSequence = 0
            self._buffer = []
            self.__refill__() #preparing for the next epoch
        else:
            batch = self._buffer[:self._batchSize]
            #remove the extracted batch from the buffer
            self._buffer = self._buffer[self._batchSize:]
        
        xw = []; xc=[]; y=[]
        for xi,yi in batch:
            #Syi=np_utils.to_categorical(yi, self._tagDictionarySize-1)
            xw.append(xi[0]); xc.append(xi[1]); y.append(yi)
        return [np.array(xw), np.array(xc)],np.expand_dims(y,2)
        
    def nBatches(self):
        return len(self._sourceIndices)//self._batchSize + 1

    def __refill__(self):
        while len(self._buffer) < constants.BUFFER_SIZE:
            if(self._currentSequence < len(self._sourceIndices)):
                sourceIndex = self._sourceIndices[self._currentSequence]
                sequence = self._sourceAccesser(0,sourceIndex)
                labelSequence = self._sourceAccesser(1, sourceIndex)
                characterSequence = self._sourceAccesser(2, sourceIndex)
#                characterSequence = [truncWord 
                nSequencePadding = self._maxSequenceLength - len(sequence)

                if nSequencePadding < 0:
                    nSequencePadding = 0
                    sequence = sequence[:self._maxSequenceLength]
                    
                paddedSequence = np.pad(sequence, 
                                        (0, nSequencePadding),
                                        'constant',
                                        constant_values = (0, constants.PADDING_CODE))
                paddedLabelSequence = np.pad(labelSequence,
                                             (0, nSequencePadding),
                                             'constant',
                                             constant_values = (0, constants.PADDING_CODE))
                
                characterSequence = [self._padWord_(word) for word in characterSequence]
                
                if len(characterSequence) > self._maxSequenceLength:
                    characterSequence = characterSequence[:self._maxSequenceLength]
                    

                paddedCharacterSequence = np.pad(characterSequence,
                                                 ((0, nSequencePadding),
                                                  (0,0)),
                                                 'constant',
                                                 constant_values = constants.PADDING_CODE)
                self._buffer.append(([paddedSequence, paddedCharacterSequence], paddedLabelSequence))
                self._currentSequence += 1
            else:
                break
    
    def _padWord_(self, x):
        if len(x) > self._maxWordLength:
            x = x[:self._maxWordLength]          
        return np.pad(x, (0, self._maxWordLength-len(x)), 'constant',constant_values = (0, constants.PADDING_CODE))

def PosInputPresenterFactory(sourceIndices, sequenceInput, accesser):
    return PosInputPresenter(constants.BATCH_SIZE, sourceIndices, accesser,
                             sequenceInput.getLabelDictionarySize(), 
                             sequenceInput.getMaxSequenceLength(),
                             sequenceInput.getMaxWordLength())
        
