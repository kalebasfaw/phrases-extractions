# -*- coding: utf-8 -*-
"""
Created on Mon May 20 19:58:22 2019

@author: Admin
"""

#Loading trained model and evaluate
import sys
import os
import numpy as np
import constants
from text_sequence_input import TextSequenceInput as Input
from posModel import PosModel
from trainedPosModel import TrainedModel
from posInput import TreebankInput, PosInputPresenter, PosInputPresenterFactory
from custom_accuracy_metrics import new_sparse_categorical_accuracy as acc

def main():
    # the NTLK Corpora path
    # path = None
    path = None
    #corpus = RowTextFile(path).getCorpus()
    corpus = TreebankInput(path).getCorpus()
    #input = Input(corpus, False, (.7,.1,.2), WordEmbeddingPresenterFactory)
    input = Input(corpus, (.7,.1,.2), PosInputPresenterFactory, isLabeled=True, includeCharacterEncoding=True)
    input.initializeWithSavedDataAndDictionaryTextSequenceInput("Dataset/pos_data_set")
    
    testPresenter = input.getTestData()
    testBatches = testPresenter.nBatches()
    
    model = TrainedModel("TrainedModels/pos_model", "adam", "sparse_categorical_crossentropy",[acc]).getModel()

    testResult = model.evaluate_generator(testPresenter, steps = testBatches)
    print(testResult)

if __name__ == '__main__':
    main()

