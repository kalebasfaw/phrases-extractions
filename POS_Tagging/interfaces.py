# -*- coding: utf-8 -*-
"""
Created on Sun Jan 20 12:20:41 2019

@author: Admin
"""
from abc import ABC, abstractmethod

class InputPresenter(ABC):
    'Represents an abstract python generator for presenting data to deep learning models'
    def __init__(self, batchSize=32):
        self._batchSize = batchSize
        super().__init__()
    
    def __iter__(self):
        
        return self
    def __next__(self):
        '''Provide the next batch of input from the underlying dataset. If there 
        remain less than batchSize in the dataset the last iteration returns 
        as much as available and resets to start from the first batch in the
        next iteration. It uses the next abstract method to fetch the actual
        batch'''
        return self._fetch_()
    @abstractmethod
    def _fetch_(self):
        '''Subclasses are expected to implement this method'''
        pass
    
    @abstractmethod
    def nBatches(self):
        'How many batches are to be presented'
        pass
    
class Input(ABC):
    '''Represents an input stream to a deep learning model. An input is 
    responsible for the logic disecting the training corupus into training, 
    validation and test datasets'''
    def __init__(self):
        super().__init__()

    @abstractmethod
    def reset(self):
        '''Marks the begining of a training session. An Input class disects
        the training curpus into training, validation and test datasets. This 
        happens at the begining of a training session or at the begining of
        a new training epoch'''
        pass
        
    @abstractmethod
    def getTrainingData(self):
        '''Returns an InputPresenter object to access the training dataset. A
        reset at the begining of a new training epoch shall automatically reset
        the InputPresenter object'''
        pass
        
    @abstractmethod
    def getTestData(self):
        '''Returns an InputPresenter object to access the test dataset. A
        reset at the begining of a new training epoch shall automatically reset
        the InputPresenter object'''
        pass
        
    @abstractmethod
    def getValidationData(self):
        '''Returns an InputPresenter object to access the validation dataset. A
        reset at the begining of a new training epoch shall automatically reset
        the InputPresenter object'''
        pass
    
    @abstractmethod
    def save(self, path):
        '''Saves the training, evaluation and test subsets to file so that
        training or evaluation could continue after the training has been
        completed, but evaluation is needed to continue on a model loaded 
        from file'''
    
    @abstractmethod
    def load(self, path):
        '''Loades the training, evaluation and test subsets from file so
        that training or evaluation is continued with a model loaded from
        file'''
        
    def _defaultCode_(self, vocabulary):
        '''Assuming words are orderd in the vocabulary with their frequency of 
        occurence in the inpuy text words are mapped to an integer corresponds 
        to their position in the dictionary'''
    
        dictionary = {vocabulary[i][0]:i+1 for i in range(len(vocabulary))}
        reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
        return dictionary, reverse_dictionary
    


class Encoder(ABC):
    def __init__(self):
        super().__init__()
        
    @abstractmethod
    def encode(self, vocabulary):
        '''Returns a dictionary and reverse dictionary of the vocabulary parameter
        mapped to vector representation of each word in the vocabulary'''
        pass
    
class ModelProvider(ABC):
    def __init__(self, model):
        self._model = model
        super().__init__()

    def getModel(self):
        '''Returns a model consisting of appropriate NN layers that could be
        trained and run to predict output'''
        return self._model
                
    
        