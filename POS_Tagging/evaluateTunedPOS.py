#Loading trained model and evaluate

from text_sequence_input import RowTextFile
from trainedPosModel import TrainedModel
from posEvaluationInput import PosEvaluationInput as Input, PosEvaluationPresenterFactory, LabledTextFile
from custom_accuracy_metrics import new_sparse_categorical_accuracy as acc

def main():
    #loading the evaluation sample data
    path = 'Corpus/190801_Tagged_Corpus _191101_FinalWork_TunerTest.txt'
    evaluationSample = LabledTextFile(path).getCorpus()
    evaluationInput = Input(True, evaluationSample, "Dataset/pos_data_set_tuned",  24, 271, PosEvaluationPresenterFactory)
    presenter = evaluationInput.getPresenter()
    
    model = TrainedModel("TrainedModels/pos_model_tuned", "adam", "sparse_categorical_crossentropy",[acc]).getModel()
    model.compile("adam", "sparse_categorical_crossentropy", [acc])
    steps = presenter.nBatches()
    evaluationResult = model.evaluate_generator(presenter,steps)
    print("Loss=" ,evaluationResult[0], " Accu=" ,evaluationResult[1])
    print(model.metrics_names)

if __name__ == '__main__':
    main()

