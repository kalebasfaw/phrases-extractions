#Loading trained model and evaluate

import constants
from text_sequence_input import RowTextFile
from trainedPosModel import TrainedModel
from posEvaluationInput import LabledTextFile
from custom_accuracy_metrics import new_sparse_categorical_accuracy as acc
from posInput import PosInputPresenterFactory
from training import Trainer
from training import TrainingStatstics as ts
from text_sequence_input import TextSequenceInput as Input
from keras import models, optimizers
from keras.models import Sequential



def main():
    #loading the evaluation sample data
    path = 'Corpus/190801_Tagged_Corpus _191101_FinalWork_Tuner.txt'
    corpus = LabledTextFile(path).getCorpus()
    input = Input(corpus, (.7,.1,.2), PosInputPresenterFactory, isLabeled=True, includeCharacterEncoding=True)
    input.initializeWithDictionary("Dataset/pos_data_set", 24, 271)
    
    print(input.getMaxWordLength(), input.getMaxSequenceLength())
    
    trainingPresenter = input.getTrainingData()
    validationPresenter = input.getValidationData()
    testPresenter = input.getTestData()
    trainingSteps = trainingPresenter.nBatches()
    validationSteps = validationPresenter.nBatches()
    testSteps = testPresenter.nBatches()
    epochs = 185 
        
    adamOpt = optimizers.Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, amsgrad=False)
    model = TrainedModel("TrainedModels/pos_model", adamOpt, "sparse_categorical_crossentropy",[acc]).getModel()
    
    
    print(model.summary())
    
    model.compile(adamOpt, "sparse_categorical_crossentropy", [acc])
    history = model.fit_generator(trainingPresenter, steps_per_epoch=trainingSteps, epochs=epochs, validation_data=validationPresenter, validation_steps=validationSteps)
    evaluationResult = model.evaluate_generator(testPresenter,testSteps)
    print("Loss=", evaluationResult[0], " Accu=", evaluationResult[1])

    # serialize model to JSON
    model_json = model.to_json()
    with open("TrainedModels/pos_model_tuned.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("TrainedModels/pos_model_tuned.h5")
    
    # save also the partitioned dataset to file so that evaluation is done on the saved models
    input.save("Dataset/pos_data_set_tuned")
    
    statstics = ts(history, model.name, "TuneTrainingStatistics", "new_sparse_categorical_accuracy")
    statstics.save()
    statstics.plot()
    statstics.saveModelVis(model)

if __name__ == '__main__':
    main()

