# -*- coding: utf-8 -*-
"""
Created on Sat Jun 22 16:34:41 2019

@author: Admin
"""

import numpy as np
import re 
import json
import constants
from interfaces import Input, InputPresenter

class PosEvaluationInput(Input):
    def __init__(self, isLabeled, evaluationSample, path, 
                 maxWordLength, maxSequenceLength,
                 presenterFactory, **kwargs):
        
        self._isLabeled = isLabeled
        if(isLabeled):
            self._evaluationSample, self._evaluationLabels = evaluationSample
        else:
            self._evaluationSample = evaluationSample
            self._evaluationLabels = None
            
        self._maxWordLength = maxWordLength
        self._maxSequenceLength = maxSequenceLength
        self._presenterFactory = presenterFactory
        
        #Loading the dictionaries
        dictList = [self._loadDictionary_(path+constants.INPUT_DICTIONARY)]
        dictList.append(self._loadDictionary_(path+constants.CHARACTER_DICTIONARY))
        dictList.append(self._loadDictionary_(path+constants.LABEL_DICTIONARY))
        self._dictionaries = tuple(dictList)
        
        
        
        #encoding the evaluation sample
        self._wordEncodedSequences = [[PosEvaluationInput._encode_(word.lower(), self._dictionaries[0][0]) for word in sequence] for sequence in self._evaluationSample]
        self._characterEncodedSequences = [[self._characterEncodedWord_(word) for word in sequence] for sequence in self._evaluationSample]
        self._sourceSample = [self._wordEncodedSequences, self._characterEncodedSequences];
        if(isLabeled):
            self._labelSequences = [[PosEvaluationInput._encode_(label, self._dictionaries[2][0]) for label in sequence] for sequence in self._evaluationLabels]
            self._sourceSample.append(self._labelSequences)

        super().__init__(**kwargs)
        
    def _loadDictionary_(self, fileWithPath):
        with open(fileWithPath, 'r') as jsonFile:
            jsonDictionary = json.load(jsonFile)
        return tuple(json.loads(jsonDictionary))
    
    def _encode_(token, dictionary):
        try:
            code = dictionary[token]
        except:
            code = constants.UNKNOWN_CODE
        return code
    
    def _characterEncodedWord_(self, word):
        if(len(word) > self._maxWordLength):
            leftCut = int(self._maxWordLength/2)
            rightCut = len(word)-self._maxWordLength+leftCut
            charEncoding = [PosEvaluationInput._encode_(word[i], self._dictionaries[1][0]) for i in range(leftCut)]
            charEncoding.extend([PosEvaluationInput._encode_(word[i], self._dictionaries[1][0]) for i in range(rightCut, len(word))])
        else:
            charEncoding = [PosEvaluationInput._encode_(word[i], self._dictionaries[1][0]) for i in range(len(word))]
        return charEncoding
    
    def isLabeled(self):
        return self._isLabeled
    
    def getSourceSize(self):
        return len(self._evaluationSample)
    
    def getMaxSequenceLength(self):
        return self._maxSequenceLength
    
    def getMaxWordLength(self):
        return self._maxWordLength
    
    def getPresenter(self):
        return self._presenterFactory(self, lambda t, x: self._sourceSample[t][x] )
            
    def getTrainingData(self):
        return None
    
    def getTestData(self):
        return None
    
    def getValidationData(self):
        return None
    
    def save(self, path):
        ret = True
        
    def load(self, path):
        ret = True
        
    def reset(self):
        ret = True
        
    def decodeOutput(self, batch, y):
        outputSize = y.shape[0]
        start = batch*constants.BATCH_SIZE
        output = []
        for i in range(start, start+outputSize):
            tokens = self._evaluationSample[i]
            tokenTagPair =[(tokens[j],self._decodeTag_(y[i][j])) for j in range(len(tokens))]
            output.append(tokenTagPair)
        return output
    
    def _decodeTag_(self, y):
        maxIndices, = np.where(y==np.max(y))
        key = str(maxIndices[0])
        tag = self._dictionaries[2][1][key]
        return tag
    
class PosEvaluationPresenter(InputPresenter):
    def __init__(self, isLabeled, batchSize, sourceSize, 
                 maxSequenceLength, maxWordLength, 
                 sourceAccessor, **kwargs):
        assert(batchSize < constants.BUFFER_SIZE) 
        self._isLabeled = isLabeled
        self._nElements = sourceSize
        self._sourceAccessor = sourceAccessor
        self._maxSequenceLength = maxSequenceLength
        self._maxWordLength = maxWordLength
        self._currentSequence = 0
        self._buffer = []
        self._refill_()
        
        super().__init__(batchSize, **kwargs)
        
    def _fetch_(self):
        if(self._batchSize > len(self._buffer)):
            self._refill_()
        if self._batchSize > len(self._buffer):
            #this must be the last batch 
            batch = self._buffer
            self._currentSequence = 0
            self._buffer = []
            self._refill_()
        else:
            batch = self._buffer[:self._batchSize]
            #remove the extracted batch from the buffer
            self._buffer = self._buffer[self._batchSize:]
            
        xw=[]; xc=[]; y=[]
        if(self._isLabeled):
            for xi,yi in batch:
                xw.append(xi[0]); xc.append(xi[1])
                y.append(yi)
            return [np.array(xw), np.array(xc)], np.expand_dims(y,2)
        else:
            for xi, in batch:
                xw.append(xi[0]); xc.append(xi[1])
            return ([np.array(xw), np.array(xc)],)

    def _refill_(self):
        while len(self._buffer) < constants.BUFFER_SIZE:
            if(self._currentSequence < self._nElements):             
                sequence = self._sourceAccessor(0,self._currentSequence)
                characterSequence = self._sourceAccessor(1, self._currentSequence)                #The number of padding required to level with the longest sequence
                nSequencePadding = self._maxSequenceLength - len(sequence)
#                If the sequence is greater the max trunc the sequnce and set the padding to zero
                if nSequencePadding < 0:
                    nSequencePadding = 0
                    sequence = sequence[:self._maxSequenceLength]
                    
                paddedSequence = np.pad(sequence, 
                                        (0, nSequencePadding),
                                        'constant',
                                        constant_values = (0, constants.PADDING_CODE))
                #Padding each sequence of characters to level with the longest words
                characterSequence = [self._padWord_(word) for word in characterSequence]
                
                if len(characterSequence) > self._maxSequenceLength:
                    characterSequence = characterSequence[:self._maxSequenceLength]
                    
                #Padding each sequence of sequence of characters to level with the longest sequence
                paddedCharacterSequence = np.pad(characterSequence,
                                                 ((0, nSequencePadding),
                                                  (0,0)),
                                                 'constant',
                                                 constant_values = constants.PADDING_CODE)
                if(self._isLabeled):
                    labelSequence = self._sourceAccessor(2, self._currentSequence)
                    #The same padding as in the input sequence
                    paddedLabelSequence = np.pad(labelSequence,
                                             (0, nSequencePadding),
                                             'constant',
                                             constant_values = (0, constants.PADDING_CODE))
                    self._buffer.append(([paddedSequence, paddedCharacterSequence], paddedLabelSequence))
                else:
                    self._buffer.append(([paddedSequence, paddedCharacterSequence],))
                
                self._currentSequence += 1
            else:
                break
    def nBatches(self):
        return self._nElements//self._batchSize + 1
    
    def _padWord_(self, x):
        if len(x) > self._maxWordLength:
            x = x[:self._maxWordLength]          
        return np.pad(x,(0, self._maxWordLength-len(x)), 'constant',constant_values = (0, constants.PADDING_CODE))
    
class POSPredictionPresenter(InputPresenter):
    def __init__(self, batchSize, sourceSize, 
                 maxSequenceLength, maxWordLength, 
                 sourceAccessor, **kwargs):
        self._presenter = PosEvaluationPresenter(False, batchSize, sourceSize, 
                 maxSequenceLength, maxWordLength, 
                 sourceAccessor)
        self._refill_()
        super().__init__(batchSize, **kwargs)
    
    def nBatches(self):
        return self._presenter.nBatches()
    
    def _refill_(self):
        self._presenter._refill_()
        
    def _fetch_(self):
        return self._presenter._fetch_()[0]
        

   
def PosEvaluationPresenterFactory(sequenceInput, accessor):
    return PosEvaluationPresenter(sequenceInput.isLabeled(), constants.BATCH_SIZE, 
                                  sequenceInput.getSourceSize(),
                             sequenceInput.getMaxSequenceLength(),
                             sequenceInput.getMaxWordLength(), accessor)
    
def PosPredictionPresenterFactory(sequenceInput, accessor):     
    return POSPredictionPresenter(constants.BATCH_SIZE, 
                                  sequenceInput.getSourceSize(),
                             sequenceInput.getMaxSequenceLength(),
                             sequenceInput.getMaxWordLength(), accessor)

class LabledTextFile:
    def __init__(self, path):
        with open(path, 'rb') as f:
            l = f.read()
            text = l.decode('utf8','ignore')
        f.close()
        self._sentences = []
        self._sentenceTags = []
        self._corpus = self._parseCorpus_(text)
        
    def _parseCorpus_(self, text):
        lines = text.split('\n')
        currentSentenceIndex = 1
        currentSentence=[]
        currentTag = []
        for line in lines:            
            parsedLine = re.findall(r'(\[(\d+)\]\((.+),(.+)\))', line)
            if(len(parsedLine) != 0):
                index = int(parsedLine[0][1])
                if index != currentSentenceIndex:
                    self._sentences.append(currentSentence)
                    self._sentenceTags.append(currentTag)
                    currentSentence = []
                    currentTag =  []
                    currentSentenceIndex = index
                currentSentence.append(parsedLine[0][2])
                currentTag.append(parsedLine[0][3])
            
    def getCorpus(self):
        return self._sentences, self._sentenceTags
        
       