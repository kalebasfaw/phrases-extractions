# -*- coding: utf-8 -*-
"""
Created on Thu Feb  7 12:41:33 2019

@author: Admin
"""
import numpy as np
from keras.utils import np_utils
import math
import re
import collections
import itertools
import json
import constants

from interfaces import Input,InputPresenter, Encoder

class DefaultTextEncoder(Encoder):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        
    def encode(self, vocabulary):
        dictionary = {vocabulary[i][0]:i for i in range(len(vocabulary))}
        reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
        return dictionary, reverse_dictionary

class WordEmbeddingPresenter(InputPresenter):
    def __init__(self, batchSize, sourceIndices, window, sourceAccessor, dictionarySize, **kwargs):
        assert(batchSize < constants.BUFFER_SIZE)
        
        # Counting howmany elements do we have across the sequences
        nElements = 0
        for index in sourceIndices:
            nElements += len(sourceAccessor(0,index))
        self._nElements = nElements
        self._sourceIndices = sourceIndices
        self._window = window
        self._sourceAccessor = sourceAccessor
        self._dictionarySize = dictionarySize
        self._currentSequence = 0
        self._buffer = []
        self.__refill__()
        
        super().__init__(batchSize, **kwargs)
        
    def _fetch_(self):
        if self._batchSize > len(self._buffer):
            #refill the buffer
            self.__refill__()
        batch = []
        if self._batchSize > len(self._buffer):
            #this must be the last batch 
            batch = self._buffer
            self._currentSequence = 0
            self._buffer = []
            self.__refill__() #preparing for the next epoch
        else:
            batch = self._buffer[:self._batchSize]
            #remove the extracted batch from the buffer
            self._buffer = self._buffer[self._batchSize:]
        
        x = []; y=[]
        for xi,yi in batch:
            x.append(xi); y.append(yi)
        return np.array(x),np.array(y)
        
    def nBatches(self):
        return self._nElements//self._batchSize + 1

    def __refill__(self):
        while len(self._buffer) < constants.BUFFER_SIZE:
            if(self._currentSequence < len(self._sourceIndices)):
                sourceIndex = self._sourceIndices[self._currentSequence]
                sequence = self._sourceAccessor(0,sourceIndex)
                sequence_length = len(sequence)
                context_words = []
                target_word   = []            
                for index, word in enumerate(sequence):
                    start = index - self._window
                    end = index + self._window + 1
            
                    context_words.append([sequence[i] 
                                         for i in range(start, end) 
                                         if 0 <= i < sequence_length 
                                         and i != index])
                    target_word.append(word)
                sequenceParts = [
                        (np.pad(
                            cw,
                            (0,2*self._window-len(cw)),
                            'constant', 
                            constant_values=(0, constants.PADDING_CODE)), 
                        target_word[i])
                    for i,cw in enumerate(context_words)]
                self._buffer.extend(sequenceParts)
                self._currentSequence += 1
            else:
                break
            
def WordEmbeddingPresenterFactory(indices, sequenceInput, sequencesAccessor):
    return WordEmbeddingPresenter(constants.BATCH_SIZE, indices, 
                                  constants.W2V_WINDOW_SIZE, sequencesAccessor, 
                                  sequenceInput.getWordDictionarySize())

  
class CbowInputPresenter(InputPresenter):
    def __init__(self, w2vPresenter, batchSize, window, 
                 dictionarySize, negativeSamplesRate = 0.25, **kwargs):    
        self._w2vPresenter = w2vPresenter;
        self._window = window
        self._dictionarySize = dictionarySize
        self._negativeSamplesRate = negativeSamplesRate
        
        super().__init__(math.floor(batchSize*(1+negativeSamplesRate)))
        
    def _fetch_(self):
            # fetch positive samples from the wrapped presenter
            # add negativeSamplesRate * nPositiveSampes nevative samples
            contexts,targets = self._w2vPresenter._fetch_()
            batchSize = self._batchSize 
            sampleSize = batchSize - len(contexts) 
            labels=[1 if l<len(contexts) else 0 
                    for l in range(0, batchSize)]
            
            if(sampleSize > 0): #do this only if we really need to add negative samples
                negSamplesRange = constants.W2V_NEGATIVE_SAMPLES_RANGE \
                    if constants.W2V_NEGATIVE_SAMPLES_RANGE < self._dictionarySize \
                    else self._dictionarySize
                negContexts = np.random.choice(range(1,negSamplesRange),
                                              (sampleSize, 2*self._window))

                negTargetsIndex = np.random.choice(range(0,len(contexts)), sampleSize)
                negTargets = targets[negTargetsIndex]
            
                contexts = np.append(contexts,negContexts, axis=0)
                targets = np.append(targets,negTargets)
            #padding the target with 2*window zerros so that it has the same dimension as the context vector
            paddedTargets=np.zeros((len(targets), 2*self._window), np.int32);
            
            for i,xi in enumerate(targets):
                paddedTargets[i][0]=xi
            return [np.array(contexts), paddedTargets], np.array(labels)

    def nBatches(self):
        return self._w2vPresenter.nBatches()
    
class TextSequenceInput(Input):
    def __init__(self, corpus,
                 partitionRatio, presenterFactory, 
                 wordEncoder=DefaultTextEncoder(), 
                 includeCharacterEncoding=False, 
                 isLabeled=True,
                 wordVocabularySize = constants.WORD_VOCABULARY_SIZE,
                 labelVocabularySize = constants.LABEL_VOCABULRY_SIZE,
                 characterVocabularySize = constants.CHARACTER_VOCABULARY_SIZE,
                 **kwargs):
        self._isLabeled = isLabeled
        self._includeCharacterEncoding = includeCharacterEncoding
        if isLabeled:
            self._rawSequences, self._rawLabels = corpus
        else:
            self._rawSequences = corpus
        self._wordSequences = [[word.lower() for word in sequence] for sequence in self._rawSequences]
#        self._maxWordLength = max([len(word) for sequence in self._wordSequences for word in sequence])
#        self._maxSequenceLength = max([len(sequence) for sequence in self._wordSequences])
        self._partitionRatio = partitionRatio
        self._presenterFactory = presenterFactory
        self._wordEncoder = wordEncoder
        self._wordVocabularySize = wordVocabularySize
        self._labelVocabularySize = labelVocabularySize
        self._characterVocabularySize = characterVocabularySize
        
            
        super().__init__(**kwargs)
    
    def _calculateSequenceLength(self, ):
        self._maxWordLength = max([len(word) for sequence in self._wordSequences for word in self._rawSequences])
        self._maxSequenceLength = max([len(sequence) for sequence in self._wordSequences])


    def _buildDictionary_(self, elementFrequencies, encoder):
        
        #Our vocabulary is, including Unknown Words and Padding
        elementVocabulary = [[constants.PADDING,1],[constants.UNKNOWN, 1]]
        elementVocabulary.extend([[element,frequence] for element,frequence in elementFrequencies])
        
        #obtain dictionary and riverse dictionary mapping words from our vocabulary 
        #to tokens
        return encoder.encode(elementVocabulary)

    def _encodeSequences_(self, sequences, encoder, dictionary):        
        encodedSequences = []
        for sequence in sequences:
            sequenceElements = []
            for element in sequence:
                try:
                    sequenceElements.append(dictionary[element])
                except:
                    sequenceElements.append(constants.UNKNOWN_CODE)
            encodedSequences.append(sequenceElements)    
        return np.array(encodedSequences)
    def initializeEmptyTextSequenceInput(self):
        self._dictionaries = []
        self.buildWordEmbeddingDictionaries()
        if(self._isLabeled):
            self.buildLabelDictionaries()
        if(self._includeCharacterEncoding):
            self.buildCharacterDictionaries()
        
        # Convert dictionaries to tuple ((wordDictionary, wordReverseDictionary),
        #   (labelDictionary, lebelReverseDictionary),(characterDictionary, characterReverseDictionary))
        self._dictionaries = tuple(self._dictionaries)
        self._calculateSequenceLength()
        self.encodeCorpus()
        self.reset()
        
    def initializeWithSavedDataAndDictionaryTextSequenceInput(self, path):
        self.load(path)
        self.encodeCorpus()
        self._calculateSequenceLength()
        
    def initializeWithDictionary(self, path, maxWordLength = None, maxSentenceLength = None):
        dictList = [self._loadDictionary_(path+constants.INPUT_DICTIONARY)]
        if(self._isLabeled):
            dictList.append(self._loadDictionary_(path+constants.LABEL_DICTIONARY))
        if(self._includeCharacterEncoding):
            dictList.append(self._loadDictionary_(path+constants.CHARACTER_DICTIONARY))
        self._dictionaries = tuple(dictList);
        if(maxWordLength == None or maxSentenceLength == None):
            self._calculateSequenceLength()
        else:
            self._maxWordLength = maxWordLength
            self._maxSequenceLength = maxSentenceLength
            
        self.encodeCorpus()
        self.reset()

    def initializeWithSavedWordEmbeddingDictionary(self, path, maxWordLength = None, maxSentenceLength = None):
        self._dictionaries = [self._loadDictionary_(path+constants.INPUT_DICTIONARY)]
        if(self._isLabeled):
            self.buildLabelDictionaries()
        if(self._includeCharacterEncoding):
            self.buildCharacterDictionaries()
        
        # Convert dictionaries to tuple ((wordDictionary, wordReverseDictionary),
        #   (labelDictionary, lebelReverseDictionary),(characterDictionary, characterReverseDictionary))
        self._dictionaries = tuple(self._dictionaries)
        if(maxWordLength == None or maxSentenceLength == None):
            self._calculateSequenceLength()
        else:
            self._maxWordLength = maxWordLength
            self._maxSequenceLength = maxSentenceLength
            
        self.encodeCorpus()
        self.reset()
        
        
    def encodeCorpus(self):
        encodedWordSequences = self._encodeSequences_(self._wordSequences, 
                                               self._wordEncoder, 
                                               self._dictionaries[0][0])
        self._sequences = [encodedWordSequences]
        #encode lables
        if(self._isLabeled):
            encodedLabels = self._encodeSequences_(self._rawLabels, 
                                                   self._wordEncoder, 
                                                   self._dictionaries[1][0])
            #labels = [np.array([label]) for label in labels]
            self._sequences.append(encodedLabels)
        #enchode character sequences
        if(self._includeCharacterEncoding):            
            charSequences = []
            for sequence in self._rawSequences:
                charSequences.append( 
                        self._encodeSequences_(
                                sequence, 
                                self._wordEncoder, 
                                self._dictionaries[2][0])) 
            self._sequences.append(np.array(charSequences))
            
        #convert input data sequences to tuple (wordEncodedSequences,labels, characterEncodedSequences)
        self._sequences = tuple(self._sequences)

        
    def buildWordEmbeddingDictionaries(self):
        #All words and vocabulary of the input
        words= list(itertools.chain.from_iterable(self._wordSequences))
        wordFrequencies = collections.Counter(words).most_common(self._wordVocabularySize-2)
        self._maxSequenceLength = max([len(sequence) for sequence in self._rawSequences])
        #self._maxSequenceLength = math.ceil(self._maxSequenceLength/4)*4
        self._maxWordLength = max([len(wordFrequency[0]) for wordFrequency in wordFrequencies])
        wordDictionary, wordReverseDictionary = \
            self._buildDictionary_(wordFrequencies, self._wordEncoder)        
        
        self._dictionaries.append((wordDictionary, wordReverseDictionary))

    def buildLabelDictionaries(self):        
        #build lables dictionaries
        labelElements = list(itertools.chain.from_iterable(self._rawLabels))
        labelFrequencies = collections.Counter(labelElements).most_common(self._labelVocabularySize-2)
        labelDictionary, labelReverseDictionary = \
            self._buildDictionary_(labelFrequencies,
                                   self._wordEncoder)
        self._dictionaries.append((labelDictionary, labelReverseDictionary))
                
    def buildCharacterDictionaries(self):
        #build character dictionaries
        #produce character based sequences
        characters = []
        for sequence in self._rawSequences:
            for word in sequence:
                characters.extend([c for c in word])
        characterFrequencies = collections.Counter(characters).most_common(self._characterVocabularySize-2)
        charDictionary, charReverseDictionary = self._buildDictionary_(characterFrequencies,
                                                   self._wordEncoder)
        self._dictionaries.append((charDictionary, charReverseDictionary))
        
    def reset(self):
        #Random indexes for training, testing and validation datasets
        #Note individual piece of data is now a sequence
        
        bins = np.random.choice(3, len(self._sequences[0]), p=list(self._partitionRatio))
        self._trainingData = np.array(np.where(bins==0)[0],dtype=np.int32)
        self._validationData = np.where(bins==1)[0]
        self._testData = np.where(bins==2)[0]
        
    def getTrainingData(self):
        return self._presenterFactory(self._trainingData, self, lambda t,x: self._sequences[t][x])
    
    def getTestData(self):
        return self._presenterFactory(self._testData, self, lambda t,x: self._sequences[t][x])
    
    def getValidationData(self):
        return self._presenterFactory(self._validationData, self, lambda t,x: self._sequences[t][x])
    
    def save(self, path):
        #save the dataset
        np.save(path+constants.TRAINING_DATASET, self._trainingData)
        np.save(path+constants.VALIDATION_DATASET, self._validationData)
        np.save(path+constants.TEST_DATASET, self._testData)
        
        #Save input and label dictionaries
        self._saveDictionary_(self._dictionaries[0], path+constants.INPUT_DICTIONARY)
        if(self._isLabeled):
            self._saveDictionary_(self._dictionaries[1], path+constants.LABEL_DICTIONARY)
        if(self._includeCharacterEncoding):
            self._saveDictionary_(self._dictionaries[2], path+constants.CHARACTER_DICTIONARY)
        
    def _saveDictionary_(self, dictionary, fileWithPath):
        jsonDictionary = json.dumps(dictionary);
        with open(fileWithPath, 'w') as jsonFile:
            json.dump(jsonDictionary, jsonFile)
        
        
    def load(self, path):
        self._trainingData = np.load(path+constants.TRAINING_DATASET)
        self._validationData = np.load(path+constants.VALIDATION_DATASET)
        self._testData = np.load(path+constants.TEST_DATASET)
        dictList = [self._loadDictionary_(path+constants.INPUT_DICTIONARY)]
        if(self._isLabeled):
            dictList.append(self._loadDictionary_(path+constants.LABEL_DICTIONARY))
        if(self._includeCharacterEncoding):
            dictList.append(self._loadDictionary_(path+constants.CHARACTER_DICTIONARY))
        self._dictionaries = tuple(dictList);
        
    def _loadDictionary_(self, fileWithPath):
        with open(fileWithPath, 'r') as jsonFile:
            jsonDictionary = json.load(jsonFile)
        return tuple(json.loads(jsonDictionary))
                    
    
    def getWords(self, codes):
        return [self._dictionaries[0][1][code] for code in codes]
    
    def getLabels(self, codes):
        return [self._dictionaries[1][1][code] for code in codes]
    
    def getWordCharacters(self,wordCodes):
        return [self._dictionaries[2][1][code] for code in wordCodes]
    
    def getWordCodes(self, words):
        return [self._dictionaries[0][0][word] for word in words]
    
    def getLabelCodes(self, words):
        return [self._dictionaries[1][0][word] for word in words]
    
    def getWordDictionarySize(self):
        return len(self._dictionaries[0][0])
    
    def getLabelDictionarySize(self):
        return len(self._dictionaries[1][0])
    
    def getCharacterDictionarySize(self):
        return len(self._dictionaries[2][0])
    
    def getMaxWordLength(self):
        return self._maxWordLength
    
    def getMaxSequenceLength(self):
        return self._maxSequenceLength
    
class RowTextFile:

    def __init__(self, path):
        with open(path, 'rb') as f:
            l = f.read()
            text = l.decode('utf8', 'ignore')
#        text = ""
#        f=open(path, 'rb')
#        index = 0
#        while(True):
#            piece = f.read(1024)
#            piece = str(piece, 'utf-8')
#            text = text + piece
#            index = index + 1
#            print("Block : " + str(index) + "\n")
#            if(len(piece) < 1024):
#                break
        #text = f.read()
        f.close()
        self._corpus = self._cleanAndCapture_(text)
        
    def _cleanAndCapture_(self, text):
        'Clean up the text from irelevant symbols and spaces'
        text = re.sub(r"([^sS])'(\s)",r'\1\2',text)
        text = re.sub(r"(\s)'(.)|^'|([^sS])'$",r'\1\2',text)
        text = re.sub(r'["\[{}<>\]]','',text)
        text = re.sub(r'(\s)[^a-zA-Z0-9](\s)',r'\1', text)
        text = re.sub(r'[.;:](\s|--end--)', r'\1', text)
        text = re.sub(r'\s{2,}',' ', text)
        #text = re.sub(r'(["\({\[<'+"'"+r'])([^\1]+)\1',r'\2', text)
        text = text.replace('(','( ')
        text = text.replace(')',' )')
        text = text.replace(',',' ,')
        sequencesRow = text.split("--end--")
        sequences = [x+' .' for x in sequencesRow if len(x) > 1]
#        return [re.sub(r'\s{2,}',' ', sequence.strip().lower()).split() 
#            for sequence in sequences if len(sequence)>0]
        
#        return [re.sub(r'\s{2,}',' ', sequence.strip()).split() 
#            for sequence in sequences if len(sequence)>0]
        aa = [re.sub(r'\s{2,}',' ', sequence.strip()).split() 
            for sequence in sequences if len(sequence)>0]
        delet_index = []
        start = -1
        bb = []
        for a in aa:
            for i in range(len(a)):
                if '(' in a[i] and '(' != a[i]:
                    start = i
                if ')' == a[i] and start != -1:
                    end = i
                    delet_index.append([start,end])
                    for i in range(start,end):
                        a[start] = a[start] + a[i+1]
                    start = -1
            delet_indexs = []
            for i in delet_index:
                delet_indexs.extend(list(range(i[0]+1,i[1]+1)))
            b = []
            for i in range(len(a)):
                if i not in delet_indexs:
                    b.append(a[i])
            bb.append(b)
        return bb
        
    def getCorpus(self):
        return self._corpus
        

        
                    
                    
        
        
        