# -*- coding: utf-8 -*-
"""
Created on Mon May 20 19:44:01 2019

@author: Admin
"""

#CBOW Word2Vectpr model training with combined Treebank and vulnerability data sets
from cbowSEmbedding import CbowSEmbedding
from text_sequence_input import TextSequenceInput as Input, WordEmbeddingPresenterFactory, RowTextFile as VulnerabilityData, CbowInputPresenter
from posInput import TreebankInput
from training import Trainer
import constants
import numpy as np
from numpy import linalg as la
import json

def oneHotCode(tokensList, dim):
    code = np.zeros((len(tokensList),dim),dtype=np.float);
    for i in range(len(tokensList)):
        code[i][tokensList[i]]=1.0
    return code;

def main():
    listOfWords = ["this", "is", "a", "simple", "list", "of", "words"]
    dictionaryFile = "Dataset/cbow_data_set_input_dictionary.txt"
    with open(dictionaryFile, 'r') as jsonFile:
        sDictTuple = json.load(jsonFile)
    dictTuple = tuple(json.loads(sDictTuple))
    dictionary = dictTuple[0]    
    weightsFile = "TrainedModels/CBOWWeights.npy"    
    weights = np.load(weightsFile)
    weights = np.asarray(weights)
    inputTokensList = [dictionary[word] for word in listOfWords]
    inputCode = oneHotCode(inputTokensList,weights.shape[0])
    embeddedVectors = np.matmul(inputCode,weights)
    context = (1/6)*(embeddedVectors[0]+embeddedVectors[1]+embeddedVectors[2]+ embeddedVectors[4]+embeddedVectors[5]+embeddedVectors[6])
    target = embeddedVectors[3]
    dif = context-target
    modl = la.norm(dif)
    print(embeddedVectors.shape)
    

if __name__ == '__main__':
    main()

