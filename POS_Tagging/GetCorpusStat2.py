# -*- coding: utf-8 -*-
"""
Created on Thu May 13 16:43:17 2021

@author: 
"""

print("Treebank Sentences: \t\t\t{}".format(3914))
print("Vulnerability description: \t\t\t{}".format(152439))
print("Total Treebank Tokens: \t\t\t{}".format(100676))
print("Total Vulnerability Tokens: \t\t\t{}".format(6167497))
print("Unique Treebank Tokens: \t\t\t{}".format(12408))
print("Unique Vulnerability Tokens: \t\t\t{}".format(244782))
print("Vulnerability Sentences: \t\t\t{}".format(275344))
