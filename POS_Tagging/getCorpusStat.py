# -*- coding: utf-8 -*-
"""
Created on Wed Jun  5 19:18:46 2019

@author: Admin
"""
from text_sequence_input import RowTextFile as VulnerabilityData
from posInput import TreebankInput
import collections

def main():
    treeBankPath = None
    vulnerabilityPath = r"D:\software\data\12\111.txt"
    treeBankCorpus,_ = TreebankInput(treeBankPath).getCorpus()
    vulnerabilityCorpus = VulnerabilityData(vulnerabilityPath).getCorpus()
    flatTreeBankCorpus = [y for x in treeBankCorpus for y in x]
    flatVulnerabilityCorpus = [y for x in vulnerabilityCorpus for y in x]
    treeBankUniqueTokens = collections.Counter(flatTreeBankCorpus).most_common(50000)
    vulnerablityUniqueTokens = collections.Counter(flatVulnerabilityCorpus).most_common(1000000)

    print("Treebank Sentences: \t\t\t{}".format(len(treeBankCorpus)))
    print("Vulnerability Sentences: \t\t{}".format(len(vulnerabilityCorpus)))
    print("Total Treebank Tokens: \t\t\t{}".format(len(flatTreeBankCorpus)))
    print("Total Vulnerability Tokens: \t\t{}".format(len(flatVulnerabilityCorpus)))
    print("Unique Treebank Tokens: \t\t{}".format(len(treeBankUniqueTokens)))
    print("Unique Vulnerability Tokens: \t\t{}".format(len(vulnerablityUniqueTokens)))

if __name__ == '__main__':
    main()


