# -*- coding: utf-8 -*-
"""
Created on Wed May 12 16:39:20 2021

@author: 
"""
import spacy
nlp = spacy.load('en_core_web_sm')



import json
cve_items = []
for i in range(2002,2022,1):
    with open(r"./nvdcve-1.1-"+str(i)+".json",'r', encoding='UTF-8') as load_f:
        load_dict = json.load(load_f)
    cve_item = load_dict['CVE_Items']
    cve_items.extend(cve_item)
data = []
j = 0
ids = []
for i in cve_items:
    print(j)
    j+=1
    id = i['cve']['CVE_data_meta']['ID']
    ids.append(id)
    descript = i['cve']['description']['description_data'][0]['value']
    data.append([id,descript])

text = ''
j = 0
k=0
for i in data:
    print(j)
    j+=1    
    # id = i[0]
    descript = i[1]
    if '** ' in descript:
        pass
    else:
# print('final_sent_numbers: ',k)
        test_doc = nlp(descript)
        for sent in test_doc.sents:
            k+=1
        text = text+ descript + '--end--'

fh = open(r'./NVD_Corpus.txt', 'w', encoding='UTF-8')
fh.write(text)
fh.close()
print('final_sent_numbers: ',k)